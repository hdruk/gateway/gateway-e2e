let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	actionBar: { btnEditResourceSelector },
} = locator;

Page.prototype.editResource = async function () {
	console.log('Editing a resource');
	btnEditResource = await this.findByCustom(btnEditResourceSelector);
	await btnEditResource.click();
};

module.exports = Page;
