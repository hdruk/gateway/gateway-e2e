let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');

const {
	relatedResourcesModal: { txtSearchBarSelector, btnAddResourcesSelector },
} = locator;
const screenshotDirectory = 'related resources modal';

let txtSearchBar, btnPapersTab, btnAddResources;

Page.prototype.addRelatedPaper = async function () {
	console.log('Adding a paper as a related resource');

	txtSearchBar = await this.findByCustom(txtSearchBarSelector);
	btnAddResources = await this.findByCustom(btnAddResourcesSelector);

	await this.write(txtSearchBar, 'Test paper created from E2E repo \n');
	// Switching to papers tab
	btnPapersTab = await this.findByCustom(By.xpath('//*[@data-test-id="related-resource-tabs"]/a[5]'));
	await btnPapersTab.click();
	// Selecting paper to add
	relatedPaperObject = await this.findByCustom(By.css('[data-test-id="related-paper-object"]'));
	await relatedPaperObject.click();

	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding related paper', screenshotDirectory);
	await btnAddResources.click();
};

module.exports = Page;
