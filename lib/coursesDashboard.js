let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	coursesDashboard: { btnAddNewCourseSelector },
} = locator;
const screenshotDirectory = 'courses dashboard';
let btnAddNewCourse;

Page.prototype.addNewCourse = async function () {
	console.log('Adding a new course');
	btnAddNewCourse = await this.findByCustom(btnAddNewCourseSelector);
	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Courses dashboard', screenshotDirectory);
	await btnAddNewCourse.click();
};

module.exports = Page;
