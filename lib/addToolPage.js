let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { TOOL_PAGE } = require('../mocks/tools');

const {
	addToolPage: {
		txtLinkSelector,
		txtNameSelector,
		acTypeSelector,
		txtDescriptionSelector,
		acImplementationSelector,
		txtVersionSelector,
		btnPublishSelector,
		txtAuthorsSelector,
		acUploadersSelector,
	},
} = locator;
const screenshotDirectory = 'add tool page';

let txtLink, txtName, acType, txtDescription, txtVersion,txtAuthors, acUploaders, btnPublish;

Page.prototype.createNewTool = async function () {
	console.log('Filling out tool form');

	txtLink = await this.findByCustom(txtLinkSelector);
	txtName = await this.findByCustom(txtNameSelector);
	txtDescription = await this.findByCustom(txtDescriptionSelector);
	acType = await this.findByCustom(acTypeSelector);
	acImplementation = await this.findByCustom(acImplementationSelector);
	txtVersion = await this.findByCustom(txtVersionSelector);
	txtAuthors = await this.findByCustom(txtAuthorsSelector);
	acUploaders = await this.findByCustom(acUploadersSelector);
	btnPublish = await this.findByCustom(btnPublishSelector);

	await this.write(txtLink, TOOL_PAGE.link);
	await this.write(txtName, TOOL_PAGE.name);
	//Type typeahead
	await this.write(acType, TOOL_PAGE.type);
	const optType = await this.findByCustom(By.id('categories.category-item-0'));
	await optType.click();

	await this.clear(txtDescription);
	await this.write(txtDescription, TOOL_PAGE.description);
	await this.write(txtAuthors, TOOL_PAGE.authors);
	//Implementation typeahead
	await this.write(acImplementation, TOOL_PAGE.implementation);
	const optImplementation = await this.findByCustom(By.id('programmingLanguage-0-item-0'));
	await optImplementation.click();
	// Uploaders typeahead
	await this.write(acUploaders, TOOL_PAGE.uploader2);
	const optUploaders = await this.findByCustom(By.id('authors-item-0'));
	await optUploaders.click();

	await this.write(txtVersion, TOOL_PAGE.version);
	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding tool', screenshotDirectory);
	await btnPublish.click();
};

module.exports = Page;
