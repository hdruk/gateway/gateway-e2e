const webdriver = require('selenium-webdriver');
const { Builder, By, until, Key } = webdriver;
const clearCache = require('selenium-chrome-clear-cache');
const locator = require('../utils/locator');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

// Load environment variables
const dotenv = require('dotenv');
dotenv.config({ path: '.env' });

// Setup chrome driver
const chrome = require('selenium-webdriver/chrome');
let o = new chrome.Options();

o.addArguments('disable-infobars');
if (process.env.RUN_HEADLESS === 'true') {
	console.log('Running headless');
	// running test on visual chrome browser
	o.addArguments('headless');
	o.addArguments('window-size=1600x900');
}
o.setUserPreferences({ credential_enable_service: false });

var Page = function () {
	let {
		common: { autocomplete },
		navbar: {
			ddAddNewEntitySelector,
			optAddCollectionSelector,
			optAddCourseSelector,
			optAddPaperSelector,
			optAddProjectSelector,
			optAddToolSelector,
			ddUserNavigationSubMenuSelector,
			ddUserNavigationDropdownSelector,
			optAccountSelector,
			optToolsSelector,
			optProjectsSelector,
			optPapersSelector,
			optCoursesSelector,
			optDatasetsSelector,
			optDataAccessRequestsSelector,
			optCollectionsSelector,
			optUsersRolesSelector,
			optLogoutSelector,
		},
		login: {
			btnLoginSelector,
			btnGoogleSignInSelector,
			txtGoogleEmailSelector,
			btnGoogleEmailNextSelector,
			txtGooglePasswordSelector,
			btnGooglePasswordNextSelector,
			txtGoogleEmailHeadlessSelector,
			btnGoogleEmailNextHeadlessSelector,
			txtGooglePasswordHeadlessSelector,
			btnGooglePasswordNextHeadlessSelector,
			btnAllowConsentSelector,
		},
	} = locator;

	let btnLogin,
		btnGoogleSignIn,
		txtGoogleEmail,
		btnGoogleEmailNext,
		txtGooglePassword,
		btnGooglePasswordNext,
		btnAllowConsent,
		ddUserNavigationDropdown,
		optAccount,
		optTools,
		optProjects,
		optPapers,
		optCourses,
		optDatasets,
		optDataAccessRequests,
		optCollections,
		optUsersRoles,
		optLogout;

	this.driver = new webdriver.Builder().setChromeOptions(o).forBrowser('chrome').build();
	this.driver.manage().setTimeouts({ implicit: 10 * 1000 });

	// visit a webpage
	this.visit = async function (url) {
		console.log(`Visiting URL ${url}`);
		return await this.driver.get(url);
	};

	// quit current session
	this.quit = async function () {
		return await this.driver.quit();
	};

	// wait and find a specific element with it's id
	this.findById = async function (id, timeout = 15000) {
		await this.driver.wait(until.elementLocated(By.id(id)), timeout, `Looking for element ${id}`);
		return await this.driver.findElement(By.id(id));
	};

	// wait and find a specific element with it's name
	this.findByName = async function (name, timeout = 15000) {
		await this.driver.wait(until.elementLocated(By.name(name)), timeout, `Looking for element ${name}`);
		return await this.driver.findElement(By.name(name));
	};

	// wait and find a specific element with a selector object
	this.findByCustom = async function (selector, timeout = 15000) {
		await this.driver.wait(until.elementLocated(selector), timeout, `Looking for element ${selector}`);
		return await this.driver.findElement(selector);
	};

	this.findAll = async function (selector, timeout = 15000) {
		await this.driver.wait(until.elementsLocated(selector), timeout, `Looking for elements ${selector}`);
		return await this.driver.findElements(selector);
	};

	//wait and find a specific element once it is enabled
	this.findWhenClickable = async function (selector, timeout = 15000) {
		const element = await this.driver.wait(until.elementLocated(selector), timeout, `Looking for element ${selector}`);
		await this.driver.wait(until.elementIsEnabled(element), timeout, `Waiting for element to enable ${selector}`);
		return await (await this.driver).findElement(selector);
	};

	// fill input web elements
	this.write = async function (element, txt) {
		return await element.sendKeys(txt);
	};

	// clear input web element
	this.clear = async function (element) {
		do {
			await element.sendKeys(Key.BACK_SPACE);
		} while ((await element.getAttribute('value')) !== '');
	};

	// implicit wait for specified amount of time
	this.wait = async function (duration) {
		await new Promise(r => setTimeout(r, duration));
	};

	// deselect the current web element by clicking on the document body
	this.blur = async function () {
		await this.driver.findElement(By.css('body')).click();
	};

	// clear Chrome browser cache
	this.clearCache = async function () {
		console.log('Clearing browser cache');
		await clearCache({ webdriver, driver: this.driver }, { history: true, cookies: true, cache: true });
	};

	// scroll down to an element on the page
	this.scrollDownTo = async function (webElement) {
		await this.driver.executeScript('arguments[0].scrollIntoView(true);', webElement);
	};

	// scroll up to an element on the page
	this.scrollUpTo = async function (webElement) {
		await this.driver.executeScript('arguments[0].scrollIntoView(false);', webElement);
	};

	this.setCheckboxValue = async function (selector, value = false) {
		await this.driver.executeScript(
			`const element = document.querySelector(arguments[0]);
	        const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "checked").set;
	        nativeInputValueSetter.call(element, ${value});
	        const inputEvent = new Event('click', { bubbles: true});
	        element.dispatchEvent(inputEvent);`,
			selector.value
		);
	};

	// clear tokens from autocomplete control
	this.clearAutocompleteTokens = async function (webElement) {
		let tokenCount = (await webElement.findElements(autocomplete.tokenBtnRemoveSelector)).length;
		let completed = tokenCount === 0;
		if (completed) return;
		do {
			const element = await webElement.findElement(autocomplete.tokenBtnRemoveSelector);
			await element.click();
			const remainingElements = await webElement.findElements(autocomplete.tokenBtnRemoveSelector);
			if (remainingElements.length === 0) {
				completed = true;
			}
		} while (!completed);
	};

	// login to the Gateway
	this.login = async function (user) {
		let screenshotDirectory = 'login';
		this.clearScreenshots(screenshotDirectory);
		// Update selectors if running headless mode
		if (this.isDriverHeadless() === true) {
			console.log('Logging in headless');
			txtGoogleEmailSelector = txtGoogleEmailHeadlessSelector;
			btnGoogleEmailNextSelector = btnGoogleEmailNextHeadlessSelector;
			txtGooglePasswordSelector = txtGooglePasswordHeadlessSelector;
			btnGooglePasswordNextSelector = btnGooglePasswordNextHeadlessSelector;
		} else {
			console.log('Logging in');
		}
		this.takeScreenshot('1. home', screenshotDirectory);

		// Find login button and click
		console.log('Finding login button');
		btnLogin = await this.findByCustom(btnLoginSelector);
		await btnLogin.click();

		this.takeScreenshot('2. choose-signin', screenshotDirectory);

		// Find Google sign in button and click
		console.log('Finding Google sign in button');
		btnGoogleSignIn = await this.findByCustom(btnGoogleSignInSelector);
		await this.wait(1000);
		await btnGoogleSignIn.click();

		// Find Google email text input and enter email address
		console.log('Finding Google email input');
		txtGoogleEmail = await this.findByCustom(txtGoogleEmailSelector);
		await this.write(txtGoogleEmail, user.emailAddress);

		this.takeScreenshot('3. entered-email', screenshotDirectory);

		// Find Google next button and click
		console.log('Finding Google email next button');
		btnGoogleEmailNext = await this.findByCustom(btnGoogleEmailNextSelector);
		await btnGoogleEmailNext.click();

		// Find Google password text input and enter password
		console.log('Finding Google password input');
		txtGooglePassword = await this.findByCustom(txtGooglePasswordSelector);
		await this.write(txtGooglePassword, user.password);

		this.takeScreenshot('4. entered-password', screenshotDirectory);

		// Find Google next button and click
		console.log('Finding Google password next button');
		btnGooglePasswordNext = await this.findByCustom(btnGooglePasswordNextSelector);
		await btnGooglePasswordNext.click();

		// Now signed in via Google
		console.log('Signed in via Google');
		this.takeScreenshot('5. logged in', screenshotDirectory);

		// Additional step required for legacy headless Google login
		if (this.isDriverHeadless()) {
			btnAllowConsent = await this.findWhenClickable(btnAllowConsentSelector);
			await btnAllowConsent.click();
			this.takeScreenshot('6.', screenshotDirectory);
		}
	};

	// log out of the Gateway
	this.logout = async function () {
		console.log('Logging out');
		optLogout = await this.findByCustom(optLogoutSelector);
		await optLogout.click();
		if (!this.isDriverHeadless()) {
			await this.clearCache();
		}
	};

	// open user navigation dropdown
	this.findAndOpenUserNavigationDropdown = async function (isAdmin) {
		console.log('Finding and opening user navigation dropdown');

		const result = await this.driver.wait(async () => {
			ddUserNavigationDropdown = await this.findByCustom(ddUserNavigationDropdownSelector);

			const ddUserNavigationDropdownDisplayed = await ddUserNavigationDropdown.isDisplayed();
			await ddUserNavigationDropdown.click();

			// Admin account is part of a team so requires extra click for user menu
			if (isAdmin) {
				ddUserNavigationSubMenu = await this.findByCustom(ddUserNavigationSubMenuSelector);
				ddUserNavigationSubMenu.click();
			}

			optAccount = await this.findByCustom(optAccountSelector);
			const optAccountDisplayed = await optAccount.isDisplayed();

			optDatasets = await this.findByCustom(optDatasetsSelector);
			const optDatasetsDisplayed = await optDatasets.isDisplayed();

			optDataAccessRequests = await this.findByCustom(optDataAccessRequestsSelector);
			const optDataAccessRequestsDisplayed = await optDataAccessRequests.isDisplayed();

			optTools = await this.findByCustom(optToolsSelector);
			const optToolsDisplayed = await optTools.isDisplayed();

			optProjects = await this.findByCustom(optProjectsSelector);
			const optProjectsDisplayed = await optProjects.isDisplayed();

			optPapers = await this.findByCustom(optPapersSelector);
			const optPapersDisplayed = await optPapers.isDisplayed();

			optCourses = await this.findByCustom(optCoursesSelector);
			const optCoursesDisplayed = await optCourses.isDisplayed();

			optCollections = await this.findByCustom(optCollectionsSelector);
			const optCollectionsDisplayed = await optCollections.isDisplayed();

			let optUsersRolesDisplayed = false;
			if (isAdmin) {
				optUsersRoles = await this.findByCustom(optUsersRolesSelector);
				optUsersRolesDisplayed = await optUsersRoles.isDisplayed();
			}

			optLogout = await this.findByCustom(optLogoutSelector);
			const optLogoutDisplayed = await optLogout.isDisplayed();

			return {
				ddDisplayed: ddUserNavigationDropdownDisplayed,
				optAccountDisplayed,
				optToolsDisplayed,
				optProjectsDisplayed,
				optPapersDisplayed,
				optCoursesDisplayed,
				optDataAccessRequestsDisplayed,
				optDatasetsDisplayed,
				optCollectionsDisplayed,
				optUsersRolesDisplayed,
				optLogoutDisplayed,
			};
		}, 50000);

		return result;
	};

	this.findAndOpenAddNewEntityDropdown = async () => {
		const result = await this.driver.wait(async () => {
			ddAddNewEntity = await this.findByCustom(ddAddNewEntitySelector);

			const addNewEntityDropdown = await ddAddNewEntity.isDisplayed();
			await ddAddNewEntity.click();

			optAddCollection = await this.findByCustom(optAddCollectionSelector);
			const optAddCollectionDisplayed = await optAddCollection.isDisplayed();

			optAddCourse = await this.findByCustom(optAddCourseSelector);
			const optAddCourseDisplayed = await optAddCourse.isDisplayed();

			optAddPaper = await this.findByCustom(optAddPaperSelector);
			const optAddPaperDisplayed = await optAddPaper.isDisplayed();

			optAddProject = await this.findByCustom(optAddProjectSelector);
			const optAddProjectDisplayed = await optAddProject.isDisplayed();

			optAddTool = await this.findByCustom(optAddToolSelector);
			const optAddToolDisplayed = await optAddTool.isDisplayed();

			return {
				addNewEntityDropdown,
				optAddCollectionDisplayed,
				optAddCourseDisplayed,
				optAddPaperDisplayed,
				optAddProjectDisplayed,
				optAddToolDisplayed,
			};
		}, 5000);
		return result;
	};

	// click navigation dropdown link
	this.clickNavigationLink = async function (link) {
		const navigationLink = await this.findByCustom(locator.navbar[`${link}`]);
		await navigationLink.click();
	};

	// check if the driver session is headless
	this.isDriverHeadless = function () {
		return process.env.RUN_HEADLESS === 'true';
	};

	this.takeScreenshot = async function (fileName = 'screenshot', path = 'default') {
		await this.driver.takeScreenshot().then(function (image) {
			if (!fs.existsSync(`./screenshots/${path}`)) {
				fs.mkdirSync(`./screenshots/${path}`, { recursive: true }, err => {
					if (err) throw err;
				});
			}
			fs.writeFileSync(`./screenshots/${path}/${fileName}-${moment().format('DD-MM-YYYY HH:mm:ss')}.png`, image, 'base64', error => {
				if (error != null) {
					console.log(`Error occurred while saving screenshot ${error}`);
				}
			});
		});
	};

	// clear screenshots from previous run
	this.clearScreenshots = async function (directory) {
		fs.rmdirSync(
			`./screenshots/${directory}`,
			{
				recursive: true,
			},
			error => {
				if (error) {
					console.log(error);
				}
			}
		);
	};
};

module.exports = Page;
