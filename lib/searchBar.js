let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');

const {
	searchBar: { txtSearchBarSelector },
} = locator;

let txtSearchBar;

Page.prototype.searchAndSelectDataset = async function () {
	console.log('Search for and select HDR dataset');

	// Searching for the HDR dataset
	txtSearchBar = await this.findByCustom(txtSearchBarSelector);
	await this.write(txtSearchBar, 'hdr uk papers & preprints \n');

	// Selecting the card title to navigate to the dataset page
	datasetResultCard = await this.findByCustom(By.css('[data-test-id="dataset-card-name"]'));
	await datasetResultCard.click();
};

module.exports = Page;
