let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { FIRST_MESSAGE } = require('../mocks/firstMessage');

const {
	enquiryMessage: {
		txtApplicantNameSelector,
		txtOrganisationSelector,
		txtEmailSelector,
		txtProjectTitleSelector,
		txtProjectAimSelector,
		txtFundingSelector,
		txtResearchBenefitsSelector,
		chkLinkedDatasetsYesSelector,
		txtLinkedDatasetsSelector,
		chkDatasetPartsNoSelector,
		btnSendMessageSelector,
	},
} = locator;

let txtApplicantName,
	txtOrganisation,
	txtEmail,
	txtProjectTitle,
	txtProjectAim,
	txtFunding,
	txtResearchBenefits,
	chkLinkedDatasetsYes,
	txtLinkedDatasets,
	chkDatasetPartsNo;

Page.prototype.completeFirstMessageForm = async function () {
	console.log('Filling out first message form');

	txtApplicantName = await this.findByCustom(txtApplicantNameSelector);
	txtOrganisation = await this.findByCustom(txtOrganisationSelector);
	txtEmail = await this.findByCustom(txtEmailSelector);
	txtProjectTitle = await this.findByCustom(txtProjectTitleSelector);
	txtProjectAim = await this.findByCustom(txtProjectAimSelector);
	txtFunding = await this.findByCustom(txtFundingSelector);
	txtResearchBenefits = await this.findByCustom(txtResearchBenefitsSelector);
	chkLinkedDatasetsYes = await this.findByCustom(chkLinkedDatasetsYesSelector);
	chkDatasetPartsNo = await this.findByCustom(chkDatasetPartsNoSelector);
	btnSendMessage = await this.findByCustom(btnSendMessageSelector);

	// Complete form
	await this.write(txtApplicantName, FIRST_MESSAGE.applicantName);
	await this.write(txtOrganisation, FIRST_MESSAGE.organisation);
	await this.write(txtEmail, FIRST_MESSAGE.email);
	await this.write(txtProjectTitle, FIRST_MESSAGE.projectTitle);
	await this.write(txtProjectAim, FIRST_MESSAGE.projectAim);
	await chkLinkedDatasetsYes.click();
	await chkDatasetPartsNo.click();
	await this.write(txtFunding, FIRST_MESSAGE.funding);
	await this.write(txtResearchBenefits, FIRST_MESSAGE.researchBenefits);
	txtLinkedDatasets = await this.findByCustom(txtLinkedDatasetsSelector);
	await this.write(txtLinkedDatasets, FIRST_MESSAGE.linkedDatasets);

	// Send Message
	await btnSendMessage.click();
};

module.exports = Page;
