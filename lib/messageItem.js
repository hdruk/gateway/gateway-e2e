let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	messageItem: { txtFirstMessageItemSelector },
} = locator;

let txtFirstMessageItem;

Page.prototype.findFirstMessageItem = async function () {
	console.log('Confirm first message has been correctly returned');

	txtFirstMessageItem = await this.findByCustom(txtFirstMessageItemSelector);

	const result = await this.driver.wait(async () => {
		const ltxtFirstMessageItemText = await txtFirstMessageItem.getText();

		return {
			message: ltxtFirstMessageItemText,
		};
	}, 5000);
	return result;
};

module.exports = Page;
