let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	collectionsDashboard: { btnAddNewCollectionSelector },
} = locator;
const screenshotDirectory = 'collections dashboard';
let btnAddNewCollection;

Page.prototype.addNewCollection = async function () {
	console.log('Adding a new collection');
	btnAddNewCollection = await this.findByCustom(btnAddNewCollectionSelector);
	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Collections dashboard', screenshotDirectory);
	await btnAddNewCollection.click();
};

module.exports = Page;
