let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	accountNavigationMenu: { btnDatasetsSelector, btnAdvancedSearchSelector },
} = locator;

Page.prototype.navigateToAdvancedSearchPage = async function () {
	console.log('Navigating to Advanced Search page');
	btnDatasets = await this.findByCustom(btnDatasetsSelector);
	await btnDatasets.click();
	btnAdvancedSearch = await this.findByCustom(btnAdvancedSearchSelector);
	await btnAdvancedSearch.click();
};

module.exports = Page;
