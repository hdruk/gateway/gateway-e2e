let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { PROJECT_PAGE } = require('../mocks/projects');

const {
	addProjectPage: {
		txtLinkSelector,
		txtTitleSelector,
		acTypeSelector,
		txtDescriptionSelector,
		txtResultsSelector,
		txtLeadResearcherSelector,
		txtAuthorsSelector,
		acKeywordsSelector,
		acDomainSelector,
		acUploadersSelector,
		btnPublishSelector,
	},
} = locator;
const screenshotDirectory = 'add project page';

let txtTitle, txtLink, acType, txtDescription, txtResults, txtLeadResearcher, txtAuthors, acKeywords, acDomain, btnPublish, acUploaders;

Page.prototype.createNewProject = async function () {
	console.log('Filling out project form');

	txtLink = await this.findByCustom(txtLinkSelector);
	txtTitle = await this.findByCustom(txtTitleSelector);
	acType = await this.findByCustom(acTypeSelector);
	txtDescription = await this.findByCustom(txtDescriptionSelector);
	txtResults = await this.findByCustom(txtResultsSelector);
	txtLeadResearcher = await this.findByCustom(txtLeadResearcherSelector);
	txtAuthors = await this.findByCustom(txtAuthorsSelector);
	acKeywords = await this.findByCustom(acKeywordsSelector);
	acDomain = await this.findByCustom(acDomainSelector);
	acUploaders = await this.findByCustom(acUploadersSelector);
	btnPublish = await this.findByCustom(btnPublishSelector);
	acUploaders = await this.findByCustom(acUploadersSelector);

	await this.write(txtLink, PROJECT_PAGE.link);
	await this.write(txtTitle, PROJECT_PAGE.title);
	await this.clear(txtDescription);
	await this.write(txtDescription, PROJECT_PAGE.description);
	await this.clear(txtResults);
	await this.write(txtResults, PROJECT_PAGE.results);
	await this.write(txtAuthors, PROJECT_PAGE.authors);
	await this.write(txtLeadResearcher, PROJECT_PAGE.leadResearcher);
	// Type typeahead
	await this.write(acType, PROJECT_PAGE.type);
	const optType = await this.findByCustom(By.id('categories.category'));
	await optType.click();
	// Keywords typeahead
	await this.write(acKeywords, PROJECT_PAGE.keywords);
	const optKeywords = await this.findByCustom(By.id('tags.features-item-0'));
	await optKeywords.click();
	// Domain typeahead
	await this.write(acDomain, PROJECT_PAGE.domain);
	const optDomain = await this.findByCustom(By.id('tags.topics-item-0'));
	await optDomain.click();
	// Uploaders typeahead
	await this.write(acUploaders, PROJECT_PAGE.uploader2);
	const optUploaders = await this.findByCustom(By.id('authors-item-0'));
	await optUploaders.click();

	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding project', screenshotDirectory);
	await btnPublish.click();
};

module.exports = Page;
