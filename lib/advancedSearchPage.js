let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	advancedSearchPage: { btnAccessAdvancedSearchSelector, btnRequestAccessSelector, btnAgreeToTermsSelector, valAgreeTermsSelector },
} = locator;
let btnAccessAdvancedSearch, btnRequestAccess, btnAgreeToTerms, valAgreeTerms;

Page.prototype.accessAdvancedSearch = async function () {
	console.log('Accessing advanced search');

	btnAccessAdvancedSearch = await this.findByCustom(btnAccessAdvancedSearchSelector);
	btnAccessAdvancedSearch.click();
};

Page.prototype.getRequestAccessModalContents = async function () {
	btnRequestAccess = await this.findByCustom(btnRequestAccessSelector);
	const result = await this.driver.wait(async () => {
		return {
			btnRequestAccessExists: !!btnRequestAccess,
		};
	}, 5000);
	return result;
};

Page.prototype.getTermsAndConditionsModalContents = async function () {
	btnAgreeToTerms = await this.findByCustom(btnAgreeToTermsSelector);
	const result = await this.driver.wait(async () => {
		return {
			btnAgreeToTermsExists: !!btnAgreeToTerms,
		};
	}, 5000);
	return result;
};

Page.prototype.getTermsAndConditionsValidation = async function () {
	valAgreeTerms = await this.findByCustom(valAgreeTermsSelector);
	const result = await this.driver.wait(async () => {
		const valAgreeTermsDisplayed = await valAgreeTerms.isDisplayed();
		return {
			valAgreeTermsDisplayed: valAgreeTermsDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.clickAgreeTermsModalButton = async function () {
	btnAgreeToTerms = await this.findByCustom(btnAgreeToTermsSelector);
	btnAgreeToTerms.click();
};

module.exports = Page;
