let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { COURSE_PAGE } = require('../mocks/courses');

const {
	addCoursePage: {
		txtTitleSelector,
		txtURLSelector,
		txtCourseProviderSelector,
		txtLocationSelector,
		txtDescriptionSelector,
		chkFlexibleDatesSelector,
		ddStudyModeSelector,
		txtDurationNumberSelector,
		optFullTimeSelector,
		ddDurationMeasureSelector,
		optDurationYearsSelector,
		txtFeeDescriptionSelector,
		txtFeeAmountSelector,
		ddFeePerSelector,
		optFeePerYearSelector,
		ddEntryLevelSelector,
		optEntryLevelBachelorsSelector,
		txtEntrySubjectSelector,
		btnPublishSelector,
	},
} = locator;
const screenshotDirectory = 'add course page';

let txtTitle,
	txtURL,
	txtCourseProvider,
	txtLocation,
	txtDescription,
	chkFlexibleDates,
	ddStudyMode,
	optFullTime,
	txtDurationNumber,
	ddDurationMeasure,
	optDurationYears,
	txtFeeDescription,
	txtFeeAmount,
	ddFeePer,
	optFeePerYear,
	ddEntryLevel,
	optEntryLevelBachelors,
	txtEntrySubject,
	btnPublish;

Page.prototype.createNewCourse = async function () {
	console.log('Filling out course form');

	txtTitle = await this.findByCustom(txtTitleSelector);
	txtURL = await this.findByCustom(txtURLSelector);
	txtCourseProvider = await this.findByCustom(txtCourseProviderSelector);
	txtLocation = await this.findByCustom(txtLocationSelector);
	txtDescription = await this.findByCustom(txtDescriptionSelector);
	chkFlexibleDates = await this.findByCustom(chkFlexibleDatesSelector);
	ddStudyMode = await this.findByCustom(ddStudyModeSelector);
	txtDurationNumber = await this.findByCustom(txtDurationNumberSelector);
	ddDurationMeasure = await this.findByCustom(ddDurationMeasureSelector);
	txtFeeDescription = await this.findByCustom(txtFeeDescriptionSelector);
	txtFeeAmount = await this.findByCustom(txtFeeAmountSelector);
	ddFeePer = await this.findByCustom(ddFeePerSelector);
	ddEntryLevel = await this.findByCustom(ddEntryLevelSelector);
	txtEntrySubject = await this.findByCustom(txtEntrySubjectSelector);
	btnPublish = await this.findByCustom(btnPublishSelector);

	await this.write(txtTitle, COURSE_PAGE.title);
	await this.write(txtURL, COURSE_PAGE.url);
	await this.write(txtCourseProvider, COURSE_PAGE.courseProvider);
	await this.write(txtLocation, COURSE_PAGE.location);
	await this.clear(txtDescription);
	await this.write(txtDescription, COURSE_PAGE.description);
	await this.scrollDownTo(chkFlexibleDates);
	await this.setCheckboxValue(chkFlexibleDatesSelector, true);

	// Select study mode
	await ddStudyMode.click();
	optFullTime = await this.findByCustom(optFullTimeSelector);
	await optFullTime.click();

	await this.write(txtDurationNumber, COURSE_PAGE.durationNumber);

	// Select duration per
	await ddDurationMeasure.click();
	optDurationYears = await this.findByCustom(optDurationYearsSelector);
	await optDurationYears.click();

	await this.write(txtFeeDescription, COURSE_PAGE.feeDescription);
	await this.write(txtFeeAmount, COURSE_PAGE.feeAmount);

	// Select fee per
	await ddFeePer.click();
	optFeePerYear = await this.findByCustom(optFeePerYearSelector);
	await optFeePerYear.click();

	// Select entry level
	await this.scrollDownTo(ddEntryLevel);
	await ddEntryLevel.click();
	optEntryLevelBachelors = await this.findByCustom(optEntryLevelBachelorsSelector);
	await optEntryLevelBachelors.click();

	await this.write(txtEntrySubject, COURSE_PAGE.entrySubject);

	await this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding course', screenshotDirectory);
	await btnPublish.click();
};

module.exports = Page;
