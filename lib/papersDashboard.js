let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	papersDashboard: { btnAddNewPaperSelector },
} = locator;
const screenshotDirectory = 'papers dashboard';
let btnAddNewPaper;

Page.prototype.addNewPaper = async function () {
	console.log('Adding a new paper');
	btnAddNewPaper = await this.findByCustom(btnAddNewPaperSelector);
	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Papers dashboard', screenshotDirectory);
	await btnAddNewPaper.click();
};

module.exports = Page;
