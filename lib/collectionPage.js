let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	collectionPage: { banCollectionAddedSelector, lblNameSelector, lblDescriptionSelector, lblKeywordsSelector, lblRelatedPaperSelector },
} = locator;
const screenshotDirectory = 'collection page';
let banCollectionAdded, lblName, lblKeywords, lblDescription, lblRelatedPaper;

Page.prototype.findCollectionDetails = async function () {
	console.log('Finding collection details');
	this.clearScreenshots(screenshotDirectory);

	banCollectionAdded = await this.findByCustom(banCollectionAddedSelector);
	lblName = await this.findByCustom(lblNameSelector);
	lblKeywords = await this.findByCustom(lblKeywordsSelector);
	lblDescription = await this.findByCustom(lblDescriptionSelector);
	lblRelatedPaper = await this.findByCustom(lblRelatedPaperSelector);

	this.takeScreenshot('1. New collection added', screenshotDirectory);

	const result = await this.driver.wait(async () => {
		const lblNameText = await lblName.getText();
		const lblDescriptionText = await lblDescription.getText();
		const lblKeywordsText = await lblKeywords.getText();
		return {
			collectionAddedBannerExists: !!banCollectionAdded,
			name: lblNameText,
			keywords: lblKeywordsText,
			description: lblDescriptionText,
			relatedPaperExists: !!lblRelatedPaper,
		};
	}, 5000);
	return result;
};

module.exports = Page;
