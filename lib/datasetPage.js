let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');

const {
	datasetPage: { btnRequestAccessSelector },
} = locator;

Page.prototype.openRequestAccessModal = async function () {
	console.log('Open the how to request access modal on the dataset page');

	btnRequestAccess = await this.findByCustom(btnRequestAccessSelector);
	await btnRequestAccess.click();
};

module.exports = Page;
