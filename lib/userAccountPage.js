let Page = require('./basePage');
const locator = require('../utils/locator');
const helper = require('../utils/helper');
const { promise } = require('selenium-webdriver');

const {
	userAccount: {
		banDevAndImprovementSelector,
		txtFirstNameSelector,
		valFirstNameSelector,
		txtLastNameSelector,
		valLastNameSelector,
		txtEmailAddressSelector,
		valEmailAddressSelector,
		acOrganisationSelector,
		txtBioSelector,
		acDomainSelector,
		txtLinkSelector,
		txtOrcidSelector,
		chkTermsConditionsSelector,
		chkOptFeedbackSelector,
		chkOptNewsSelector,
		btnSaveUserAccountSelector,
	},
} = locator;

let banDevAndImprove,
	txtFirstName,
	valFirstName,
	txtLastName,
	valLastName,
	txtEmailAddress,
	valEmailAddress,
	acOrganisation,
	txtBio,
	acDomain,
	txtLink,
	txtOrcid,
	chkTermsConditions,
	chkOptFeedback,
	chkOptNews,
	btnSaveUserAccount;

Page.prototype.findUserProfileDetails = async function () {
	console.log('Finding user profile details');
	banDevAndImprove = await this.findByCustom(banDevAndImprovementSelector);
	txtFirstName = await this.findByCustom(txtFirstNameSelector);
	txtLastName = await this.findByCustom(txtLastNameSelector);
	txtEmailAddress = await this.findByCustom(txtEmailAddressSelector);

	const result = await this.driver.wait(async () => {
		const txtFirstNameText = await txtFirstName.getAttribute('value');
		const txtLastNameText = await txtLastName.getAttribute('value');
		const txtEmailAddressText = await txtEmailAddress.getAttribute('value');

		return {
			firstName: txtFirstNameText,
			lastName: txtLastNameText,
			emailAddress: txtEmailAddressText,
			banDevAndImproveExists: !!banDevAndImprove,
		};
	}, 5000);
	return result;
};

Page.prototype.clearFormData = async function () {
	console.log('Finding and removing form data');
	txtFirstName = await this.findByCustom(txtFirstNameSelector);
	await this.clear(txtFirstName);

	txtLastName = await this.findByCustom(txtLastNameSelector);
	await this.clear(txtLastName);

	txtEmailAddress = await this.findByCustom(txtEmailAddressSelector);
	await this.clear(txtEmailAddress);

	acOrganisation = await this.findByCustom(acOrganisationSelector);
	await this.clear(acOrganisation);

	txtBio = await this.findByCustom(txtBioSelector);
	await this.clear(txtBio);

	acDomain = await this.findByCustom(acDomainSelector);
	await this.clearAutocompleteTokens(acDomain);

	txtLink = await this.findByCustom(txtLinkSelector);
	await this.clear(txtLink);

	txtOrcid = await this.findByCustom(txtOrcidSelector);
	await this.clear(txtOrcid);

	chkOptFeedback = await this.findByCustom(chkOptFeedbackSelector);
	await this.scrollDownTo(chkOptFeedback);
	const chkOptFeedbackChecked = JSON.parse(await chkOptFeedback.getAttribute('checked'));

	if (chkOptFeedbackChecked) {
		await this.setCheckboxValue(chkOptFeedbackSelector);
	}

	chkOptNews = await this.findByCustom(chkOptNewsSelector);
	await this.scrollDownTo(chkOptNews);
	const chkOptNewsChecked = JSON.parse(await chkOptNews.getAttribute('checked'));

	if (chkOptNewsChecked) {
		await this.setCheckboxValue(chkOptNewsSelector);
	}

	chkTermsConditions = await this.findByCustom(chkTermsConditionsSelector);
	await this.scrollDownTo(chkTermsConditions);
	const chkTermsConditionsChecked = JSON.parse(await chkTermsConditions.getAttribute('checked'));

	if (chkTermsConditionsChecked) {
		await this.setCheckboxValue(chkTermsConditionsSelector);
	}

	await this.blur();
};

Page.prototype.submitSaveForm = async function () {
	btnSaveUserAccount = await this.findByCustom(btnSaveUserAccountSelector);
	await btnSaveUserAccount.click();
};

Page.prototype.getFormValidState = async function () {
	valFirstName = await this.findByCustom(valFirstNameSelector);
	valLastName = await this.findByCustom(valLastNameSelector);
	valEmailAddress = await this.findByCustom(valEmailAddressSelector);

	const result = await this.driver.wait(async () => {
		const valFirstNameText = await valFirstName.getText();
		const valFirstNameDisplayed = await valFirstName.isDisplayed();
		const valLastNameText = await valLastName.getText();
		const valLastNameDisplayed = await valLastName.isDisplayed();
		const valEmailAddressText = await valEmailAddress.getText();
		const valEmailAddressDisplayed = await valEmailAddress.isDisplayed();

		return {
			firstName: { displayed: valFirstNameDisplayed, text: valFirstNameText },
			lastName: { displayed: valLastNameDisplayed, text: valLastNameText },
			emailAddress: { displayed: valEmailAddressDisplayed, text: valEmailAddressText },
		};
	}, 5000);
	return result;
};

module.exports = Page;
