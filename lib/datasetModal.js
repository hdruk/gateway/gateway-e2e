let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');

const {
	datasetModal: { btnMakeAnEnquirySelector },
} = locator;

Page.prototype.makeAnEnquiry = async function () {
	console.log('Click make an enquiry button to open messaging panel');

	btnMakeAnEnquiry = await this.findByCustom(btnMakeAnEnquirySelector);
	await btnMakeAnEnquiry.click();
};

module.exports = Page;
