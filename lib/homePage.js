let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	navbar: { lblUserNameSelector, imgNotificationBadgeSelector, imgMessageBadgeSelector },
} = locator;

let lblUserName, imgNotificationBadge, imgMessageBadge;

Page.prototype.findUsername = async function () {
    console.log('Finding username');
	lblUserName = await this.findByCustom(lblUserNameSelector);

	const result = await this.driver.wait(async () => {
		const lblUserNameText = await lblUserName.getText();
		const lblUserNameDisplayed = await lblUserName.isDisplayed();

		return {
			labelText: lblUserNameText,
			labelDisplayed: lblUserNameDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findNotificationBadge = async function () {
    console.log('Finding notification badge');
	imgNotificationBadge = await this.findByCustom(imgNotificationBadgeSelector);

	const result = await this.driver.wait(async () => {
		const imgNotificationBadgeDisplayed = await imgNotificationBadge.isDisplayed();
		return {
			imgDisplayed: imgNotificationBadgeDisplayed,
		};
	}, 5000);

	return result;
};

Page.prototype.findMessageBadge = async function () {
    console.log('Finding message badge');
	imgMessageBadge = await this.findByCustom(imgMessageBadgeSelector);

	const result = await this.driver.wait(async () => {
		const imgMessageBadgeDisplayed = await imgMessageBadge.isDisplayed();
		return {
			imgDisplayed: imgMessageBadgeDisplayed,
		};
	}, 5000);

	return result;
};

module.exports = Page;
