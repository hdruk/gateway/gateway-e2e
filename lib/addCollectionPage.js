let Page = require('./basePage');
let RelatedResourcesModal = require('./relatedResourcesModal');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { COLLECTION_PAGE } = require('../mocks/collections');

const {
	addCollectionPage: { txtNameSelector, txtDescriptionSelector, acKeywordsSelector, btnPublishSelector, btnAddRelatedResourceSelector },
} = locator;
const screenshotDirectory = 'add collection page';

let txtName, txtDescription, acKeywords, btnPublish, btnRelatedResource, txtSearchBarSelector;

Page.prototype.populateCollectionForm = async function () {
	console.log('Filling out collection form');

	txtName = await this.findByCustom(txtNameSelector);
	txtDescription = await this.findByCustom(txtDescriptionSelector);
	acKeywords = await this.findByCustom(acKeywordsSelector);
	btnPublish = await this.findByCustom(btnPublishSelector);
	btnRelatedResource = await this.findByCustom(btnAddRelatedResourceSelector);

	await this.write(txtName, COLLECTION_PAGE.name);
	await this.write(txtDescription, COLLECTION_PAGE.description);
	// Keywords typeahead
	await this.write(acKeywords, COLLECTION_PAGE.keywords);
	const optKeywords = await this.findByCustom(By.id('keywords-item-0'));
	await optKeywords.click();

	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding collection', screenshotDirectory);
};

Page.prototype.openRelatedResourcesModal = async function () {
	await btnRelatedResource.click();
};

Page.prototype.publishCollection = async function () {
	await btnPublish.click();
};

module.exports = Page;
