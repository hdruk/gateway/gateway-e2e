let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	toolPage: {
		banToolAddedSelector,
		banToolPendingReviewSelector,
		lblNameSelector,
		lblURLSelector,
		lblDescriptionSelector,
		lblTypeSelector,
		lblAuthorsSelector,
		lblImplementationSelector,
		lblVersionSelector,
		lblUploaders1Selector,
		lblUploaders2Selector
	},
} = locator;
const screenshotDirectory = 'tool page';
let banToolAdded, banToolPendingReview, lblName, lblURL, lblDescription, lblType, lblAuthors, lblImplementation, lblVersion, lblUploader1, lblUploader2;

Page.prototype.findToolDetails = async function () {
	console.log('Finding tool details');
	this.clearScreenshots(screenshotDirectory);

	banToolAdded = await this.findByCustom(banToolAddedSelector);
	banToolPendingReview = await this.findByCustom(banToolPendingReviewSelector);
	lblName = await this.findByCustom(lblNameSelector);
	lblURL = await this.findByCustom(lblURLSelector);
	lblDescription = await this.findByCustom(lblDescriptionSelector);
	lblType = await this.findByCustom(lblTypeSelector);
	lblAuthors = await this.findByCustom(lblAuthorsSelector);
	lblImplementation = await this.findByCustom(lblImplementationSelector);
	lblVersion = await this.findByCustom(lblVersionSelector);
	lblUploader1 = await this.findByCustom(lblUploaders1Selector);
	lblUploader2 = await this.findByCustom(lblUploaders2Selector);
	this.takeScreenshot('1. New tool added', screenshotDirectory);

	const result = await this.driver.wait(async () => {
		const lblDescriptionText = await lblDescription.getText();
		const lblNameText = await lblName.getText();
		const lblURLText = await lblURL.getAttribute('href');
		const lblTypeText = await lblType.getText();
		const lblAuthorsText = await lblAuthors.getText();
		const lblImplementationText = await lblImplementation.getText();
		const lblVersionText = await lblVersion.getText();
		const lblUploader1Text = await lblUploader1.getText();
		const lblUploader2Text = await lblUploader2.getText();
		return {
			toolAddedBannerExists: !!banToolAdded,
			toolPendingReviewBannerExists: !!banToolPendingReview,
			name: lblNameText,
			description: lblDescriptionText,
			type: lblTypeText,
			url: lblURLText,
			authors: lblAuthorsText,
			implementation: lblImplementationText,
			version: lblVersionText,
			uploader1: lblUploader1Text,
			uploader2: lblUploader2Text,
		};
	}, 5000);
	return result;
};

module.exports = Page;
