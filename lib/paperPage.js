let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	paperPage: {
		banPaperPendingReviewSelector,
		lblNameSelector,
		lblAuthorsSelector,
		lblJournalSelector,
		lblLink1Selector,
		lblLink2Selector,
		lblLink3Selector,
		lblYearSelector,
		lblAbstractSelector,
		lblResultsSelector,
		lblKeywords1Selector,
		lblDomain1Selector,
		lblUploaders1Selector,
		lblUploaders2Selector
	},
} = locator;
const screenshotDirectory = 'paper page';
let banPaperPendingReview, lblName, lblAuthors, lblJournal, lblYear, lblAbstract, lblResults, lblKeywords1, lblDomain1, lblLink1, lblLink2, lblLink3, lblUploader1, lblUploader2;

Page.prototype.findPaperDetails = async function () {
	console.log('Finding paper details');
	this.clearScreenshots(screenshotDirectory);

	banPaperPendingReview = await this.findByCustom(banPaperPendingReviewSelector);
	lblName = await this.findByCustom(lblNameSelector);
	lblAuthors = await this.findByCustom(lblAuthorsSelector);
	lblJournal = await this.findByCustom(lblJournalSelector);
	lblYear = await this.findByCustom(lblYearSelector);
	lblAbstract = await this.findByCustom(lblAbstractSelector);
	lblResults = await this.findByCustom(lblResultsSelector);
	lblKeywords1 = await this.findByCustom(lblKeywords1Selector);
	lblDomain1 = await this.findByCustom(lblDomain1Selector);
	lblLink1 = await this.findByCustom(lblLink1Selector);
	lblLink2 = await this.findByCustom(lblLink2Selector);
	lblLink3 = await this.findByCustom(lblLink3Selector);
	lblUploader1 = await this.findByCustom(lblUploaders1Selector);
	lblUploader2 = await this.findByCustom(lblUploaders2Selector);

	this.takeScreenshot('1. New paper added', screenshotDirectory);

	const result = await this.driver.wait(async () => {
		const lblTitleText = await lblName.getText();
		const lblAuthorsText = await lblAuthors.getText();
		const lblJournalText = await lblJournal.getText();
		const lblYearText = await lblYear.getText();
		const lblAbstractText = await lblAbstract.getText();
		const lblResultsText = await lblResults.getText();
		const lblKeywords1Text = await lblKeywords1.getText();
		const lblDomain1Text = await lblDomain1.getText();
		const lblLink1Text = await lblLink1.getText();
		const lblLink2Text = await lblLink2.getText();
		const lblLink3Text = await lblLink3.getText();
		const lblUploader1Text = await lblUploader1.getText();
		const lblUploader2Text = await lblUploader2.getText();
		return {
			paperPendingReviewBannerExists: !!banPaperPendingReview,
			title: lblTitleText,
			authors: lblAuthorsText,
			journal: lblJournalText,
			year: lblYearText,
			abstract: lblAbstractText,
			results: lblResultsText,
			keywords: lblKeywords1Text,
			domain: lblDomain1Text,
			link1: lblLink1Text,
			link2: lblLink2Text,
			link3: lblLink3Text,
			uploader1: lblUploader1Text,
			uploader2: lblUploader2Text,
		};
	}, 5000);
	return result;
};

module.exports = Page;
