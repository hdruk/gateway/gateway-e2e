let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	toolsDashboard: { btnAddNewToolSelector },
} = locator;
const screenshotDirectory = 'tools dashboard';
let btnAddNewTool;

Page.prototype.addNewTool = async function () {
	console.log('Adding a new tool');
	btnAddNewTool = await this.findByCustom(btnAddNewToolSelector);
	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Tools dashboard', screenshotDirectory);
	await btnAddNewTool.click();
};

module.exports = Page;
