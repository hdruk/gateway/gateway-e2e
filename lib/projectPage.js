let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	projectPage: {
		banProjectPendingReviewSelector,
		lblLinkSelector,
		lblNameSelector,
		lblTypeSelector,
		lblDescriptionSelector,
		lblResultsSelector,
		lblLeadResearcherSelector,
		lblAuthorsSelector,
		lblKeywords1Selector,
		lblDomain1Selector,
		lblUploaders1Selector,
		lblUploaders2Selector,
	},
} = locator;
const screenshotDirectory = 'project page';
let banProjectPendingReview,
	lblName,
	lblType,
	lblAuthors,
	lblDescription,
	lblResults,
	lblKeywords1,
	lblDomain1,
	lblLink,
	lblUploader1,
	lblUploader2;

Page.prototype.findProjectDetails = async function () {
	console.log('Finding project details');
	this.clearScreenshots(screenshotDirectory);

	banProjectPendingReview = await this.findByCustom(banProjectPendingReviewSelector);
	lblLink = await this.findByCustom(lblLinkSelector);
	lblName = await this.findByCustom(lblNameSelector);
	lblType = await this.findByCustom(lblTypeSelector);
	lblDescription = await this.findByCustom(lblDescriptionSelector);
	lblResults = await this.findByCustom(lblResultsSelector);
	lblLeadResearcher = await this.findByCustom(lblLeadResearcherSelector);
	lblAuthors = await this.findByCustom(lblAuthorsSelector);
	lblKeywords1 = await this.findByCustom(lblKeywords1Selector);
	lblDomain1 = await this.findByCustom(lblDomain1Selector);
	lblUploader1 = await this.findByCustom(lblUploaders1Selector);
	lblUploader2 = await this.findByCustom(lblUploaders2Selector);

	this.takeScreenshot('1. New project added', screenshotDirectory);

	const result = await this.driver.wait(async () => {
		const lblLinkText = await lblLink.getText();
		const lblNameText = await lblName.getText();
		const lblTypeText = await lblType.getText();
		const lblAuthorsText = await lblAuthors.getText();
		const lblDescriptionText = await lblDescription.getText();
		const lblResultsText = await lblResults.getText();
		const lblLeadResearcherText = await lblLeadResearcher.getText();
		const lblKeywords1Text = await lblKeywords1.getText();
		const lblDomain1Text = await lblDomain1.getText();
		const lblUploader1Text = await lblUploader1.getText();
		const lblUploader2Text = await lblUploader2.getText();
		return {
			projectPendingReviewBannerExists: !!banProjectPendingReview,
			link: lblLinkText,
			name: lblNameText,
			type: lblTypeText,
			description: lblDescriptionText,
			results: lblResultsText,
			leadResearcher: lblLeadResearcherText,
			authors: lblAuthorsText,
			keywords: lblKeywords1Text,
			domain: lblDomain1Text,
			uploader1: lblUploader1Text,
			uploader2: lblUploader2Text,
		};
	}, 5000);
	return result;
};

module.exports = Page;
