let Page = require('./basePage');
const locator = require('../utils/locator');
const helper = require('../utils/helper');
const { promise } = require('selenium-webdriver');

const {
	dashboard: {
		lblDashboardDatasetCountSelector,
		lblDashboardDatasetMetadataPercentSelector,
		lblDashboardUsersMonthlyCountSelector,
		lblDashboardUsersRegisteredPercentSelector,
		lblDashboardSearchesResultsPercentSelector,
		lblDashboardDataAccessRequestsCountSelector,
		lblDashboardGatewayUptimePercentSelector,
		lblDashboardMetricsLastUpdatedSelector,
		lstDashboardPopularDatasetsSelector,
		lblDashboardPopularDatasetsDatasetNameSelector,
		lblDashboardPopularDatasetsDatasetCustodianSelector,
		lblDashboardPopularDatasetsDatasetCountSelector,
		lstDashboardPopularSearchesSelector,
		lblDashboardPopularSearchTermSelector,
		lblDashboardPopularSearchCountSelector,
		lblDashboardPopularSearchResultsSelector,
		lstDashboardUnmetDemandSearchesSelector,
		lblDashboardUnmetDemandSearchTermSelector,
		lblDashboardUnmetDemandSearchCountSelector,
		lblDashboardUnmetDemandSearchResultsSelector,
		btnDashboardUnmetDemandDatasetsTabSelector,
		btnDashboardUnmetDemandToolsTabSelector,
		btnDashboardUnmetDemandProjectsTabSelector,
		btnDashboardUnmetDemandCoursesTabSelector,
		btnDashboardUnmetDemandPapersTabSelector,
		btnDashboardUnmetDemandPeopleTabSelector,
	},
} = locator;

let lblDashboardDatasetCount,
	lblDashboardDatasetMetadataPercent,
	lblDashboardUsersMonthlyCount,
	lblDashboardUsersRegisteredPercent,
	lblDashboardSearchesResultsPercent,
	lblDashboardDataAccessRequestsCount,
	lblDashboardGatewayUptimePercent,
	lblDashboardMetricsLastUpdated,
	lstDashboardPopularDatasets,
	lstDashboardPopularSearches,
	lstDashboardUnmetDemandSearches,
	btnDashboardUnmetDemandSearchesTab;

Page.prototype.findDatasetsCount = async function () {
	console.log('Finding dataset count');
	lblDashboardDatasetCount = await this.findByCustom(lblDashboardDatasetCountSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardDatasetCountText = await lblDashboardDatasetCount.getText();
		const lblDashboardDatasetCountDisplayed = await lblDashboardDatasetCount.isDisplayed();

		return {
			labelText: lblDashboardDatasetCountText,
			labelDisplayed: lblDashboardDatasetCountDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findDatasetMetadataPercentage = async function () {
	console.log('Finding dataset metadata percentage');
	lblDashboardDatasetMetadataPercent = await this.findByCustom(lblDashboardDatasetMetadataPercentSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardDatasetMetadataPercentText = await lblDashboardDatasetMetadataPercent.getText();
		const lblDashboardDatasetMetadataPercentDisplayed = await lblDashboardDatasetMetadataPercent.isDisplayed();

		return {
			labelText: lblDashboardDatasetMetadataPercentText.replace('%', ''),
			labelDisplayed: lblDashboardDatasetMetadataPercentDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findUsersMonthlyCount = async function () {
	console.log('Finding users monthly count');
	lblDashboardUsersMonthlyCount = await this.findByCustom(lblDashboardUsersMonthlyCountSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardUsersMonthlyCountText = await lblDashboardUsersMonthlyCount.getText();
		const lblDashboardUsersMonthlyCountDisplayed = await lblDashboardUsersMonthlyCount.isDisplayed();

		return {
			labelText: lblDashboardUsersMonthlyCountText,
			labelDisplayed: lblDashboardUsersMonthlyCountDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findUsersRegisteredPercentage = async function () {
	console.log('Finding users registered percentage');
	lblDashboardUsersRegisteredPercent = await this.findByCustom(lblDashboardUsersRegisteredPercentSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardUsersRegisteredPercentText = await lblDashboardUsersRegisteredPercent.getText();
		const lblDashboardUsersRegisteredPercentDisplayed = await lblDashboardUsersRegisteredPercent.isDisplayed();

		return {
			labelText: lblDashboardUsersRegisteredPercentText.replace('%', ''),
			labelDisplayed: lblDashboardUsersRegisteredPercentDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findSearchResultsPercentage = async function () {
	console.log('Finding search results percentage');
	lblDashboardSearchesResultsPercent = await this.findByCustom(lblDashboardSearchesResultsPercentSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardSearchesResultsPercentText = await lblDashboardSearchesResultsPercent.getText();
		const lblDashboardSearchesResultsPercentDisplayed = await lblDashboardSearchesResultsPercent.isDisplayed();

		return {
			labelText: lblDashboardSearchesResultsPercentText.replace('%', ''),
			labelDisplayed: lblDashboardSearchesResultsPercentDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findNewDataAccessRequestCount = async function () {
	console.log('Finding new data access request count');
	lblDashboardDataAccessRequestsCount = await this.findByCustom(lblDashboardDataAccessRequestsCountSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardDataAccessRequestsCountText = await lblDashboardDataAccessRequestsCount.getText();
		const lblDashboardDataAccessRequestsCountDisplayed = await lblDashboardDataAccessRequestsCount.isDisplayed();

		return {
			labelText: lblDashboardDataAccessRequestsCountText,
			labelDisplayed: lblDashboardDataAccessRequestsCountDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findGatewayUptimePercentage = async function () {
	console.log('Finding Gateway uptime percentage');
	lblDashboardGatewayUptimePercent = await this.findByCustom(lblDashboardGatewayUptimePercentSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardGatewayUptimePercentText = await lblDashboardGatewayUptimePercent.getText();
		const lblDashboardGatewayUptimePercentDisplayed = await lblDashboardGatewayUptimePercent.isDisplayed();

		return {
			labelText: lblDashboardGatewayUptimePercentText.replace('%', ''),
			labelDisplayed: lblDashboardGatewayUptimePercentDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findMetricsLastUpdated = async function () {
	console.log('Finding last updated label for metrics');
	lblDashboardMetricsLastUpdated = await this.findByCustom(lblDashboardMetricsLastUpdatedSelector, 60000);

	const result = await this.driver.wait(async () => {
		const lblDashboardMetricsLastUpdatedText = await lblDashboardMetricsLastUpdated.getText();
		const lblDashboardMetricsLastUpdatedDisplayed = await lblDashboardMetricsLastUpdated.isDisplayed();

		return {
			labelText: lblDashboardMetricsLastUpdatedText,
			labelDisplayed: lblDashboardMetricsLastUpdatedDisplayed,
		};
	}, 5000);
	return result;
};

Page.prototype.findPopularDatasets = async function () {
	console.log('Finding popular datasets');
	lstDashboardPopularDatasets = this.findAll(lstDashboardPopularDatasetsSelector, 60000);

	const result = await this.driver.wait(async () => {
		return await promise.map(lstDashboardPopularDatasets, async item => {
			const datasetName = await item.findElement(lblDashboardPopularDatasetsDatasetNameSelector);
			const datasetNameText = await datasetName.getText();
			const link = await datasetName.getAttribute('href');
			const linkAlive = await helper.isLinkAlive(link);

			const datasetCustodianName = await item.findElement(lblDashboardPopularDatasetsDatasetCustodianSelector);
			const datasetCustodianNameText = await datasetCustodianName.getText();

			const datasetCount = await item.findElement(lblDashboardPopularDatasetsDatasetCountSelector);
			const datasetCountText = await datasetCount.getText();

			return {
				datasetNameText,
				datasetCustodianNameText,
				datasetCountText,
				linkAlive,
			};
		});
	}, 5000);

	return result;
};

Page.prototype.findPopularSearches = async function () {
	console.log('Finding popular searches');
	lstDashboardPopularSearches = this.findAll(lstDashboardPopularSearchesSelector, 60000);

	const result = await this.driver.wait(async () => {
		return await promise.map(lstDashboardPopularSearches, async item => {
			const searchTerm = await item.findElement(lblDashboardPopularSearchTermSelector);
			const searchTermText = await searchTerm.getText();
			const link = await searchTerm.getAttribute('href');
			const linkAlive = await helper.isLinkAlive(link);

			const searchCount = await item.findElement(lblDashboardPopularSearchCountSelector);
			const searchCountText = await searchCount.getText();

			const latestResults = await item.findElement(lblDashboardPopularSearchResultsSelector);
			const latestResultsText = await latestResults.getText();

			return {
				searchTermText,
				searchCountText,
				latestResultsText,
				linkAlive,
			};
		});
	}, 5000);

	return result;
};

Page.prototype.findUnmetDemandSearches = async function (entity = 'Datasets') {
	let btnDashboardUnmetDemandSearchesTabSelector = null;
	console.log(`Switching unmet demand tab to entity: ${entity}`);
	switch (entity) {
		case 'Datasets':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandDatasetsTabSelector;
			break;
		case 'Tools':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandToolsTabSelector;
			break;
		case 'Projects':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandProjectsTabSelector;
			break;
		case 'Courses':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandCoursesTabSelector;
			break;
		case 'Papers':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandPapersTabSelector;
			break;
		case 'People':
			btnDashboardUnmetDemandSearchesTabSelector = btnDashboardUnmetDemandPeopleTabSelector;
			break;
	}

    btnDashboardUnmetDemandSearchesTab = await this.findWhenClickable(btnDashboardUnmetDemandSearchesTabSelector);
    await this.scrollDownTo(btnDashboardUnmetDemandSearchesTab);
	await btnDashboardUnmetDemandSearchesTab.click();
    await this.wait(1 * 500);

	console.log('Finding unmet demand searches');
	lstDashboardUnmetDemandSearches = await this.findAll(lstDashboardUnmetDemandSearchesSelector, 60000);

	const result = await this.driver.wait(async () => {
		return await promise.map(lstDashboardUnmetDemandSearches, async item => {
			const searchTerm = await item.findElement(lblDashboardUnmetDemandSearchTermSelector);
			const searchTermText = await searchTerm.getText();
			const link = await searchTerm.getAttribute('href');
			const linkAlive = await helper.isLinkAlive(link);

			const searchCount = await item.findElement(lblDashboardUnmetDemandSearchCountSelector);
			const searchCountText = await searchCount.getText();

			const searchResults = await item.findElement(lblDashboardUnmetDemandSearchResultsSelector);
			const searchResultsText = await searchResults.getText();

			return {
				searchTermText,
				searchCountText,
				searchResultsText,
				linkAlive,
			};
		});
	}, 5000);

	return result;
};

module.exports = Page;
