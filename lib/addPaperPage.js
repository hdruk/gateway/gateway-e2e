let Page = require('./basePage');
const locator = require('../utils/locator');
const { By } = require('selenium-webdriver');
const { PAPER_PAGE } = require('../mocks/papers');

const {
	addPaperPage: {
		txtTitleSelector,
		txtLink1Selector,
		txtLink2Selector,
		txtLink3Selector,
		btnAddLink1Selector,
		btnAddLink2Selector,
		txtAuthorsSelector,
		txtJournalSelector,
		txtYearSelector,
		txtAbstractSelector,
		txtResultsSelector,
		acKeywordsSelector,
		acDomainSelector,
		acUploadersSelector,
		btnPublishSelector,
	},
	editPaperPage: { acKeyword1Selector, acDomain1Selector, acUploader1Selector, acUploader2Selector },
} = locator;
const screenshotDirectory = 'add paper page';

let txtTitle, txtLink1, txtLink2, txtLink3, btnAddLink1, txtAuthors, txtJournal, txtYear, txtAbstract, txtResults, acKeywords, acDomain, btnPublish, acUploaders;

Page.prototype.createNewPaper = async function () {
	console.log('Filling out paper form');

	txtTitle = await this.findByCustom(txtTitleSelector);
	txtLink1 = await this.findByCustom(txtLink1Selector);
	btnAddLink1 = await this.findByCustom(btnAddLink1Selector);
	await btnAddLink1.click();
	btnAddLink2 = await this.findByCustom(btnAddLink2Selector);
	await btnAddLink2.click();
	txtLink2 = await this.findByCustom(txtLink2Selector);
	txtLink3 = await this.findByCustom(txtLink3Selector);
	txtJournal = await this.findByCustom(txtJournalSelector);
	txtAuthors = await this.findByCustom(txtAuthorsSelector);
	txtYear = await this.findByCustom(txtYearSelector);
	txtAbstract = await this.findByCustom(txtAbstractSelector);
	txtResults = await this.findByCustom(txtResultsSelector);
	acKeywords = await this.findByCustom(acKeywordsSelector);
	acDomain = await this.findByCustom(acDomainSelector);
	btnPublish = await this.findByCustom(btnPublishSelector);
	acUploaders = await this.findByCustom(acUploadersSelector);

	await this.write(txtTitle, PAPER_PAGE.title);
	await this.write(txtLink1, PAPER_PAGE.link1);
	await this.write(txtLink2, PAPER_PAGE.link2);
	await this.write(txtLink3, PAPER_PAGE.link3);
	await this.write(txtAuthors, PAPER_PAGE.authors);
	await this.write(txtJournal, PAPER_PAGE.journal);
	await this.write(txtYear, PAPER_PAGE.year);
	await this.clear(txtAbstract);
	await this.write(txtAbstract, PAPER_PAGE.abstract);
	await this.clear(txtResults);
	await this.write(txtResults, PAPER_PAGE.results);
	// Keywords typeahead
	await this.write(acKeywords, PAPER_PAGE.keywords);
	const optKeywords = await this.findByCustom(By.id('tags.features-item-0'));
	await optKeywords.click();
	// Domain typeahead
	await this.write(acDomain, PAPER_PAGE.domain);
	const optDomain = await this.findByCustom(By.id('tags.topics-item-0'));
	await optDomain.click();
	// Uploaders typeahead
	await this.write(acUploaders, PAPER_PAGE.uploader2);
	const optUploaders = await this.findByCustom(By.id('authors-item-0'));
	await optUploaders.click();

	this.clearScreenshots(screenshotDirectory);
	this.takeScreenshot('1. Adding paper', screenshotDirectory);
	await btnPublish.click();
};

Page.prototype.findEditPaperDetails = async function () {
	console.log('Finding edit paper details');
	txtTitleValue = await (await this.findByCustom(txtTitleSelector)).getAttribute('value');
	txtLink1Value = await (await this.findByCustom(txtLink1Selector)).getAttribute('value');
	txtLink2Value = await (await this.findByCustom(txtLink2Selector)).getAttribute('value');
	txtLink3Value = await (await this.findByCustom(txtLink3Selector)).getAttribute('value');
	txtAuthorsValue = await (await this.findByCustom(txtAuthorsSelector)).getAttribute('value');
	txtJournalValue = await (await this.findByCustom(txtJournalSelector)).getAttribute('value');
	txtYearValue = await (await this.findByCustom(txtYearSelector)).getAttribute('value');
	txtAbstractValue = await (await this.findByCustom(txtAbstractSelector)).getAttribute('value');
	txtResultsValue = await (await this.findByCustom(txtResultsSelector)).getAttribute('value');
	acKeyword1Value = await (await this.findByCustom(acKeyword1Selector)).getAttribute('option');
	acDomain1Value = await (await this.findByCustom(acDomain1Selector)).getAttribute('option');
	acUploader1Value = await (await this.findByCustom(acUploader1Selector)).getText();
	acUploader2Value = await (await this.findByCustom(acUploader2Selector)).getText();

	const result = await this.driver.wait(async () => {
		return {
			titleValue: txtTitleValue,
			link1Value: txtLink1Value,
			link2Value: txtLink2Value,
			link3Value: txtLink3Value,
			authorsValue: txtAuthorsValue,
			journalValue: txtJournalValue,
			yearValue: txtYearValue,
			abstractValue: txtAbstractValue,
			resultsValue: txtResultsValue,
			keywordsValue: acKeyword1Value,
			domainValue: acDomain1Value,
			uploader1Value: acUploader1Value,
			uploader2Value: acUploader2Value,
		};
	}, 5000);
	return result;
};

module.exports = Page;
