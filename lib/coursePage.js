let Page = require('./basePage');
const locator = require('../utils/locator');

const {
	coursePage: {
		lblTitleSelector,
		lblURLSelector,
		lblProviderSelector,
		lblDescriptionSelector,
		lblLocationSelector,
		lblCourseDateSelector,
		lblCourseDurationSelector,
		lblCourseFeesSelector,
		lblEntryLevelSelector,
		lblEntrySubjectSelector,
	},
} = locator;
const screenshotDirectory = 'course page';
let lblTitle, lblURL, lblProvider, lblDescription, lblLocation;

Page.prototype.findCourseDetails = async function () {
	console.log('Finding course details');
	this.clearScreenshots(screenshotDirectory);

	lblTitle = await this.findByCustom(lblTitleSelector);
	lblURL = await this.findByCustom(lblURLSelector);
	lblProvider = await this.findByCustom(lblProviderSelector);
	lblDescription = await this.findByCustom(lblDescriptionSelector);
	lblLocation = await this.findByCustom(lblLocationSelector);
	lblCourseDate = await this.findByCustom(lblCourseDateSelector);
	lblCourseDuration = await this.findByCustom(lblCourseDurationSelector);
	lblCourseFees = await this.findByCustom(lblCourseFeesSelector);
	lblEntryLevel = await this.findByCustom(lblEntryLevelSelector);
	lblEntrySubject = await this.findByCustom(lblEntrySubjectSelector);

	this.takeScreenshot('1. New course added', screenshotDirectory);

	const result = await this.driver.wait(async () => {
		const lblTitleText = await lblTitle.getText();
		const lblURLText = await lblURL.getText();
		const lblProviderText = await lblProvider.getText();
		const lblDescriptionText = await lblDescription.getText();
		const lblLocationText = await lblLocation.getText();
		const lblCourseDateText = await lblCourseDate.getText();
		const lblCourseDurationText = await lblCourseDuration.getText();
		const lblCourseFeesText = await lblCourseFees.getText();
		const lblEntryLevelText = await lblEntryLevel.getText();
		const lblEntrySubjectText = await lblEntrySubject.getText();

		return {
			title: lblTitleText,
			url: lblURLText,
			provider: lblProviderText,
			description: lblDescriptionText,
			location: lblLocationText,
			date: lblCourseDateText,
			duration: lblCourseDurationText,
			fees: lblCourseFeesText,
			entryLevel: lblEntryLevelText,
			entrySubject: lblEntrySubjectText,
		};
	}, 5000);
	return result;
};

module.exports = Page;
