const { describe, it } = require('mocha');
const Page = require('../lib/addProjectPage');
const ProjectPage = require('../lib/projectPage');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { PROJECT_PAGE } = require('../mocks/projects');

process.on('unhandledRejection', () => {});

(async function projectsDashboardTest() {
	try {
		describe('User can add a new project', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenAddNewEntityDropdown();
				await page.clickNavigationLink('optAddProjectSelector');
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can add a new project', async () => {
				await page.createNewProject();
				const result = await page.findProjectDetails();

				expect(result.projectPendingReviewBannerExists).to.equal(true);
				expect(result.link).to.equal(PROJECT_PAGE.link);
				expect(result.name).to.equal(PROJECT_PAGE.title);
				expect(result.type).to.equal(PROJECT_PAGE.type);
				expect(result.description).to.equal(PROJECT_PAGE.description);
				expect(result.results).to.equal(PROJECT_PAGE.results);
				expect(result.leadResearcher).to.equal(PROJECT_PAGE.leadResearcher);
				expect(result.authors).to.equal(PROJECT_PAGE.authors);
				expect(result.keywords).to.equal(PROJECT_PAGE.keywords);
				expect(result.domain).to.equal(PROJECT_PAGE.domain);
				expect(result.uploader1).to.equal(PROJECT_PAGE.uploader1);
				expect(result.uploader2).to.equal(PROJECT_PAGE.uploader2);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
