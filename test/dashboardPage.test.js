const { describe, it } = require('mocha');
const Page = require('../lib/dashboardPage');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function dashboardPageTests() {
	try {
		describe('Public dashboard loads and displays expected metrics', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;
			const screenshotDirectory = 'dashboard';

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				page.clearScreenshots(screenshotDirectory);
				driver.navigate().to(`${baseUrl}/dashboard`);
				await page.takeScreenshot('1. dashboard', screenshotDirectory);
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('Displays the total datasets', async function () {
				const result = await page.findDatasetsCount();
				await page.takeScreenshot('2. total-datasets', screenshotDirectory);
				console.log(`Total datasets: ${result.labelText}`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseInt(result.labelText)).to.be.above(0);
			});

			it('Displays the percentage of datasets with technical metadata', async function () {
				const result = await page.findDatasetMetadataPercentage();
				await page.takeScreenshot('3. percentage-datasets', screenshotDirectory);
				console.log(`Datasets with technical metadata: ${result.labelText}%`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(0);
			});

			it('Displays the number of users that have visited the site in the last month', async function () {
				const result = await page.findUsersMonthlyCount();
				await page.takeScreenshot('4. total-users', screenshotDirectory);
				console.log(`Users that have visited the site in the last month: ${result.labelText}`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(-1);
			});

			it('Displays the percentage of new users that have registered in the last month', async function () {
				const result = await page.findUsersRegisteredPercentage();
				await page.takeScreenshot('5. percentage-users', screenshotDirectory);
				console.log(`Percentage of visiting users who registered in the last month: ${result.labelText}%`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(-1);
			});

			it('Displays the percentage of searches that returned results in the last month', async function () {
				const result = await page.findSearchResultsPercentage();
				await page.takeScreenshot('6. percentage-searches', screenshotDirectory);
				console.log(`Percentage of searches returning results in the last month: ${result.labelText}%`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(-1);
			});

			it('Displays the number of new data access requests submitted in the last month', async function () {
				const result = await page.findNewDataAccessRequestCount();
				await page.takeScreenshot('7. total-access-requests', screenshotDirectory);
				console.log(`Number of new data access requests in the last month: ${result.labelText}`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(-1);
			});

			it('Displays the percentage of uptime the Gateway has had in the last month', async function () {
				const result = await page.findGatewayUptimePercentage();
				await page.takeScreenshot('8. percentage-uptime', screenshotDirectory);
				console.log(`Percentage of Gateway uptime in the last month: ${result.labelText}%`);
				expect(result.labelDisplayed).to.equal(true);
				expect(parseFloat(result.labelText)).to.be.above(0);
			});

			it('Displays the date and time the metrics were last updated', async function () {
				const result = await page.findMetricsLastUpdated();
				await page.takeScreenshot('9. last-updated-metrics', screenshotDirectory);
				console.log(`Metrics label: ${result.labelText}`);
				expect(result.labelDisplayed).to.equal(true);
				expect(result.labelText).to.contain('Last updated');
			});

			// Commenting this out as data access requests are only made on Production environment
			// it("Displays the most popular datasets by the number of access requests", async function () {
			// 	const result = await page.findPopularDatasets();
			// 	await page.takeScreenshot('10. popular-datasets', screenshotDirectory);
			// 	console.log(`Most popular datasets:`);
			// 	expect(result.length).to.be.above(0);
			// 	result.forEach((dataset) => {
			// 		console.log(`Dataset: ${dataset.datasetNameText} - Custodian: ${dataset.datasetCustodianNameText} - Access Requests: ${dataset.datasetCountText}`);
			// 		expect(dataset.datasetNameText.length).to.be.above(0);
			// 		expect(dataset.datasetCustodianNameText.length).to.be.above(0);
			// 		expect(parseInt(dataset.datasetCountText)).to.be.above(0);
			// 		expect(dataset.linkAlive).to.equal(true);
			// 	});
			// });

			it('Displays the most popular searches performed by users', async function () {
				const result = await page.findPopularSearches();
				await page.takeScreenshot('11. popular-searches', screenshotDirectory);
				console.log(`Most popular searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Results: ${search.latestResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.latestResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});

			it('Displays unmet demand searches for datasets by default where no results were returned from user searches', async function () {
				const result = await page.findUnmetDemandSearches();
				await page.takeScreenshot('12. unmet-demand-searches-default', screenshotDirectory);
				console.log(`Unmet demand searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Dataset Results: ${search.searchResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.searchResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});

			it('Displays unmet demand searches for Tools when the Tools tab is clicked', async function () {
				const result = await page.findUnmetDemandSearches('Tools');
				await page.takeScreenshot('13. unmet-demand-searches-tools', screenshotDirectory);
				console.log(`Unmet demand searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Tools Results: ${search.searchResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.searchResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});

			it('Displays unmet demand searches for Projects when the Projects tab is clicked', async function () {
				const result = await page.findUnmetDemandSearches('Projects');
				await page.takeScreenshot('14. unmet-demand-searches-projects', screenshotDirectory);
				console.log(`Unmet demand searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Projects Results: ${search.searchResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.searchResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});

			// -- Removed until unmet demand searches for courses exist or a place holder text is added --
			// it("Displays unmet demand searches for Courses when the Courses tab is clicked", async function () {
			// 	const result = await page.findUnmetDemandSearches('Courses');
			// 	await page.takeScreenshot('15. unmet-demand-searches-courses', screenshotDirectory);
			// 	console.log(`Unmet demand searches:`);
			// 	expect(result.length).to.be.above(0);
			// 	result.forEach((search) => {
			// 		console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Courses Results: ${search.searchResultsText}`);
			// 		expect(search.searchTermText.length).to.be.above(0);
			// 		expect(search.searchResultsText.length).to.be.above(0);
			// 		expect(parseInt(search.searchCountText)).to.be.above(0);
			// 		expect(search.linkAlive).to.equal(true);
			// 	});
			// });

			// -- Removed until unmet demand searches for papers exist or a place holder test is added --
			// it('Displays unmet demand searches for Papers when the Papers tab is clicked', async function () {
			// 	const result = await page.findUnmetDemandSearches('Papers');
			// 	await page.takeScreenshot('16. unmet-demand-searches-papers', screenshotDirectory);
			// 	console.log(`Unmet demand searches:`);
			// 	expect(result.length).to.be.above(0);
			// 	result.forEach(search => {
			// 		console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Papers Results: ${search.searchResultsText}`);
			// 		expect(search.searchTermText.length).to.be.above(0);
			// 		expect(search.searchResultsText.length).to.be.above(0);
			// 		expect(parseInt(search.searchCountText)).to.be.above(0);
			// 		expect(search.linkAlive).to.equal(true);
			// 	});
			// });

			it('Displays unmet demand searches for People when the People tab is clicked', async function () {
				const result = await page.findUnmetDemandSearches('People');
				await page.takeScreenshot('17. unmet-demand-searches-people', screenshotDirectory);
				console.log(`Unmet demand searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - People Results: ${search.searchResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.searchResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});

			it('Displays unmet demand searches for Datasets when the Datasets tab is clicked', async function () {
				const result = await page.findUnmetDemandSearches('Datasets');
				await page.takeScreenshot('18. unmet-demand-searches-datasets', screenshotDirectory);
				console.log(`Unmet demand searches:`);
				expect(result.length).to.be.above(0);
				result.forEach(search => {
					console.log(`Term: ${search.searchTermText} - Count: ${search.searchCountText} - Dataset Results: ${search.searchResultsText}`);
					expect(search.searchTermText.length).to.be.above(0);
					expect(search.searchResultsText.length).to.be.above(0);
					expect(parseInt(search.searchCountText)).to.be.above(0);
					expect(search.linkAlive).to.equal(true);
				});
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
