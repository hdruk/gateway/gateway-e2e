const { describe, it } = require('mocha');
const users = require('../mocks/users');
const Page = require('../lib/homePage');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function homePageTests() {
	try {
		describe('Navigation bar displays correctly for user after login', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;
			const screenshotDirectory = 'home page';

			// Before each test runs, visit the Gateway
			this.beforeEach(async () => {
				page = new Page();
				driver = page.driver;
				page.clearScreenshots(screenshotDirectory);
				await page.visit(baseUrl);
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('Displays the expected navigation items', async function () {
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				page.takeScreenshot('1. home', screenshotDirectory);

				const username = await page.findUsername();
				expect(username.labelText).to.include('Test Account');
				expect(username.labelDisplayed).to.equal(true);

				const notificationBadge = await page.findNotificationBadge();
				expect(notificationBadge.imgDisplayed).to.equal(true);

				const messageBadge = await page.findMessageBadge();
				expect(messageBadge.imgDisplayed).to.equal(true);
			});

			it('Provides a navigation dropdown with the correct links for a non admin user', async function () {
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				page.takeScreenshot('1. home', screenshotDirectory);

				const result = await page.findAndOpenUserNavigationDropdown(false);
				expect(result.ddDisplayed).to.equal(true);
				expect(result.optAccountDisplayed).to.equal(true);
				expect(result.optToolsDisplayed).to.equal(true);
				expect(result.optDatasetsDisplayed).to.equal(true);
				expect(result.optProjectsDisplayed).to.equal(true);
				expect(result.optPapersDisplayed).to.equal(true);
				expect(result.optCoursesDisplayed).to.equal(true);
				expect(result.optDataAccessRequestsDisplayed).to.equal(true);
				expect(result.optCollectionsDisplayed).to.equal(true);
				expect(result.optUsersRolesDisplayed).to.equal(false);
				expect(result.optLogoutDisplayed).to.equal(true);
				page.takeScreenshot('2. navigation-open', screenshotDirectory);
			});

			it('Provides a navigation dropdown with the correct links for an admin user', async function () {
				const user = { ...users['ADMIN'], password: process.env.ADMINGOOGLEPWD };
				await page.login(user);
				page.takeScreenshot('1. home-admin', screenshotDirectory);

				const result = await page.findAndOpenUserNavigationDropdown(true);
				expect(result.ddDisplayed).to.equal(true);
				expect(result.optAccountDisplayed).to.equal(true);
				expect(result.optToolsDisplayed).to.equal(true);
				expect(result.optDatasetsDisplayed).to.equal(true);
				expect(result.optProjectsDisplayed).to.equal(true);
				expect(result.optPapersDisplayed).to.equal(true);
				expect(result.optCoursesDisplayed).to.equal(true);
				expect(result.optDataAccessRequestsDisplayed).to.equal(true);
				expect(result.optCollectionsDisplayed).to.equal(true);
				expect(result.optUsersRolesDisplayed).to.equal(true);
				expect(result.optLogoutDisplayed).to.equal(true);
				page.takeScreenshot('2. navigation-open-admin', screenshotDirectory);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
