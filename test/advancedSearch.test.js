const { describe, it } = require('mocha');
const Page = require('../lib/toolsDashboard');
const AccountPage = require('../lib/accountPage');
const AdvancedSearchPage = require('../lib/advancedSearchPage');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');

process.on('unhandledRejection', () => {});

(async function advancedSearchTest() {
	try {
		describe('User gets Request Access Modal', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;
			const screenshotDirectory = 'advanced search page';

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				page.clearScreenshots(screenshotDirectory);
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optToolsSelector');
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can request access', async () => {
				await page.navigateToAdvancedSearchPage();
				await page.accessAdvancedSearch();
				await page.takeScreenshot('1. Clicked Access the advanced search tool', screenshotDirectory);
				const result = await page.getRequestAccessModalContents();
				await page.takeScreenshot('2. Getting Request Access modal contents', screenshotDirectory);

				expect(result.btnRequestAccessExists).to.equal(true);
			});
		});

		describe('Open Athens user goes straight to Terms & Conditions', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;
			let user;
			const screenshotDirectory = 'advanced search page (Open Athens)';

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				page.clearScreenshots(screenshotDirectory);
				await page.visit(baseUrl);
				user = { ...users['OA_USER'], password: process.env.USEROAPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optToolsSelector');
				await page.navigateToAdvancedSearchPage();
				await page.accessAdvancedSearch();
				await page.takeScreenshot('1. Clicked Access the advanced search tool', screenshotDirectory);
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User gets T&Cs modal', async () => {
				const result = await page.getTermsAndConditionsModalContents();
				await page.takeScreenshot('2. Getting T&C modal contents', screenshotDirectory);

				expect(result.btnAgreeToTermsExists).to.equal(true);
			});

			it("T&Cs modal displays the correct validation messages if user hasn't checked accept terms checkbox", async function () {
				await page.clickAgreeTermsModalButton();

				const result = await page.getTermsAndConditionsValidation();
				await page.takeScreenshot('3. T&C modal validation', screenshotDirectory);
				expect(result.valAgreeTermsDisplayed).to.equal(true);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
