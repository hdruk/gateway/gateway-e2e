const { describe, it } = require('mocha');
const Page = require('../lib/papersDashboard');
const AddPaperPage = require('../lib/addPaperPage');
const PaperPage = require('../lib/paperPage');
const CollectionsDashboard = require('../lib/collectionsDashboard');
const AddCollectionPage = require('../lib/addCollectionPage');
const CollectionPage = require('../lib/collectionPage');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { COLLECTION_PAGE } = require('../mocks/collections');

process.on('unhandledRejection', () => {});

(async function collectionsPageTest() {
	try {
		describe('User can add a paper to a new collection', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);

				// Add a new paper which will be a related resource
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optPapersSelector');
				await page.addNewPaper();
				await page.createNewPaper();
				await page.findPaperDetails();
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can add a new collection with a related paper', async () => {
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optCollectionsSelector');
				await page.addNewCollection();
				await page.populateCollectionForm();
				await page.openRelatedResourcesModal();
				await page.addRelatedPaper();
				await page.publishCollection();

				const result = await page.findCollectionDetails();
				expect(result.collectionAddedBannerExists).to.equal(true);
				expect(result.name).to.equal(COLLECTION_PAGE.name);
				expect(result.description).to.equal(COLLECTION_PAGE.description);
				expect(result.keywords).to.equal(COLLECTION_PAGE.keywords);
				expect(result.relatedPaperExists).to.equal(true);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
