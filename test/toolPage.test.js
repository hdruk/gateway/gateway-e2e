const { describe, it } = require('mocha');
const Page = require('../lib/toolsDashboard');
const AddToolPage = require('../lib/addToolPage');
const ToolPage = require('../lib/toolPage');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { TOOL_PAGE } = require('../mocks/tools');

process.on('unhandledRejection', () => {});

(async function toolsDashboardTest() {
	try {
		describe('User can add a new tool', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optToolsSelector');
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can add a new tool', async () => {
				await page.addNewTool();
				await page.createNewTool();
				const result = await page.findToolDetails();
				expect(result.toolAddedBannerExists).to.equal(true);
				expect(result.toolPendingReviewBannerExists).to.equal(true);
				expect(result.name).to.equal(TOOL_PAGE.name);
				expect(result.url).to.equal(TOOL_PAGE.link);
				expect(result.type).to.equal(TOOL_PAGE.type);
				expect(result.description).to.equal(TOOL_PAGE.description);
				expect(result.authors).to.equal(TOOL_PAGE.authors);
				expect(result.implementation).to.equal(TOOL_PAGE.implementation);
				expect(result.version).to.equal(TOOL_PAGE.version);
				expect(result.uploader1).to.equal(TOOL_PAGE.uploader1);
				expect(result.uploader2).to.equal(TOOL_PAGE.uploader2);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
