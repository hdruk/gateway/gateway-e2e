const { describe, it } = require('mocha');
const Page = require('../lib/coursesDashboard');
const AddPaperPage = require('../lib/addCoursePage');
const PaperPage = require('../lib/coursePage');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { COURSE_PAGE } = require('../mocks/courses');

process.on('unhandledRejection', () => {});

(async function papersDashboardTest() {
	try {
		describe('User can add a new course', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optCoursesSelector');
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can add a new course', async () => {
				await page.addNewCourse();
				await page.createNewCourse();
				const result = await page.findCourseDetails();

				expect(result.title).to.equal(COURSE_PAGE.title);
				expect(result.url).to.equal(COURSE_PAGE.url);
				expect(result.provider).to.equal(COURSE_PAGE.courseProvider);
				expect(result.location).to.equal(COURSE_PAGE.location);
				expect(result.description).to.equal(COURSE_PAGE.description);
				expect(result.date).to.equal(COURSE_PAGE.date);
				expect(result.duration).to.equal(`${COURSE_PAGE.studyMode} | ${COURSE_PAGE.durationNumber} ${COURSE_PAGE.durationMeasure}`);
				expect(result.fees).to.equal(`Cost of course | £${COURSE_PAGE.feeAmount} per ${COURSE_PAGE.feePerLabel}`);
				expect(result.entryLevel).to.equal(COURSE_PAGE.entryLevel);
				expect(result.entrySubject).to.equal(COURSE_PAGE.entrySubject);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
