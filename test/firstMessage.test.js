const { describe, it } = require('mocha');
const Page = require('../lib/papersDashboard');
const SearchBar = require('../lib/searchBar');
const DatasetPage = require('../lib/datasetPage');
const DatasetModal = require('../lib/datasetModal');
const EnquiryMessage = require('../lib/enquiryMessage');
const MessageItem = require('../lib/messageItem');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { MESSAGE_ITEM } = require('../mocks/firstMessage');

process.on('unhandledRejection', () => {});

(async function firstMessageTest() {
	try {
		describe('User can select a dataset and send an enquiry', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can complete the first message enquiry form for a selected dataset then view the correctly formatted message sent', async () => {
				// Search for a specific dataset and navigate to that dataset page
				await page.searchAndSelectDataset();

				// Open 'How to request access' modal
				await page.openRequestAccessModal();

				// Click 'Make an enquiry' to open messaging panel
				await page.makeAnEnquiry();

				// Fill in first message form and send
				await page.completeFirstMessageForm();

				// Confirm resulting message appears and is correct
				const result = await page.findFirstMessageItem();
				expect(result.message).to.equal(MESSAGE_ITEM.message);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
