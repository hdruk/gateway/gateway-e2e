const { describe, it } = require('mocha');
const Page = require('../lib/papersDashboard');
const AddPaperPage = require('../lib/addPaperPage');
const PaperPage = require('../lib/paperPage');
const ActionBar = require('../lib/actionBar');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.use(chaiAsPromised);
const users = require('../mocks/users');
const { PAPER_PAGE } = require('../mocks/papers');

process.on('unhandledRejection', () => {});

(async function papersDashboardTest() {
	try {
		describe('User can add a new paper', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optPapersSelector');
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('User can add a new paper', async () => {
				await page.addNewPaper();
				await page.createNewPaper();
				const result = await page.findPaperDetails();

				expect(result.paperPendingReviewBannerExists).to.equal(true);
				expect(result.title).to.equal(PAPER_PAGE.title);
				expect(result.link1).to.equal(PAPER_PAGE.link1);
				expect(result.link2).to.equal(PAPER_PAGE.link2);
				expect(result.link3).to.equal(PAPER_PAGE.link3);
				expect(result.authors).to.equal(PAPER_PAGE.authors);
				expect(result.journal).to.equal(PAPER_PAGE.journal);
				expect(result.year).to.equal(PAPER_PAGE.year);
				expect(result.keywords).to.equal(PAPER_PAGE.keywords);
				expect(result.domain).to.equal(PAPER_PAGE.domain);
				expect(result.abstract).to.equal(PAPER_PAGE.abstract);
				expect(result.results).to.equal(PAPER_PAGE.results);
				expect(result.uploader1).to.equal(PAPER_PAGE.uploader1);
				expect(result.uploader2).to.equal(PAPER_PAGE.uploader2);
			});

			it('Edit paper page displays correct values', async () => {
				await page.editResource();
				const result = await page.findEditPaperDetails();
				expect(result.titleValue).to.equal(PAPER_PAGE.title);
				expect(result.link1Value).to.equal(PAPER_PAGE.link1);
				expect(result.link2Value).to.equal(PAPER_PAGE.link2);
				expect(result.link3Value).to.equal(PAPER_PAGE.link3);
				expect(result.authorsValue).to.equal(PAPER_PAGE.authors);
				expect(result.journalValue).to.equal(PAPER_PAGE.journal);
				expect(result.yearValue).to.equal(PAPER_PAGE.year);
				expect(result.abstractValue).to.equal(PAPER_PAGE.abstract);
				expect(result.resultsValue).to.equal(PAPER_PAGE.results);
				expect(result.keywordsValue).to.equal(PAPER_PAGE.keywords);
				expect(result.domainValue).to.equal(PAPER_PAGE.domain);
				expect(result.uploader1Value.startsWith(PAPER_PAGE.uploader1)).to.equal(true);
				expect(result.uploader2Value.startsWith(PAPER_PAGE.uploader2)).to.equal(true);
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
