const { describe, it } = require('mocha');
const users = require('../mocks/users');
const Page = require('../lib/userAccountPage');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function userAccountPageTests() {
	try {
		describe('User account profile displays details of logged in user and can be updated', function () {
			this.timeout(50000);
			let driver, page;
			const baseUrl = process.env.BASE_URL;
			const screenshotDirectory = 'user account';

			// Before tests run, perform log in
			this.beforeAll(async () => {
				page = new Page();
				driver = page.driver;
				page.clearScreenshots(screenshotDirectory);
				await page.visit(baseUrl);
				const user = { ...users['USER'], password: process.env.USERGOOGLEPWD };
				await page.login(user);
				await page.findAndOpenUserNavigationDropdown(false);
				await page.clickNavigationLink('optAccountSelector');
				await page.takeScreenshot('1. user account profile', screenshotDirectory);
			});
			// After tests finish, close the browser
			this.afterAll(async () => {
				await page.quit();
			});

			it('Displays the users name and email address', async function () {
				const result = await page.findUserProfileDetails();
				await page.takeScreenshot('2. users-bio-details', screenshotDirectory);
				console.log(`Users names and email address: ${result.firstName} ${result.lastName} - ${result.emailAddress}`);

				expect(result.firstName).to.equal(users['USER'].firstName);
				expect(result.lastName).to.equal(users['USER'].lastName);
				expect(result.emailAddress).to.equal(users['USER'].emailAddress);
				expect(result.banDevAndImproveExists).to.equal(true);
			});

			it('Displays the correct validation messages when form fields are empty', async function () {
				await page.clearFormData();
				await page.takeScreenshot('3. users-bio-empty-fields', screenshotDirectory);
				await page.submitSaveForm();
				await page.takeScreenshot('4. users-bio-empty-fields-validation', screenshotDirectory);
				console.log(`Form validation state: `);
				const result = await page.getFormValidState();
				Object.keys(result).forEach(key => {
					console.log(`Field: ${key} - Displayed: ${result[key].displayed} - Message: ${result[key].text}`);
					expect(result[key].displayed).to.equal(true);
					expect(result[key].text).to.not.equal('');
				});
			});
		});
	} catch (ex) {
		console.log(new Error(ex.message));
	}
})();
