const { By } = require('selenium-webdriver');

const common = {
	autocomplete: {
		tokenSelector: By.className('rbt-token'),
		tokenBtnRemoveSelector: By.className('rbt-token-remove-button'),
	},
};

const login = {
	btnLoginSelector: By.css('[data-test-id="btnLogin"]'),
	btnGoogleSignInSelector: By.css('[data-testid="google"]'),
	txtGoogleEmailSelector: By.xpath('//*[@id="identifierId"]'),
	txtGoogleEmailHeadlessSelector: By.xpath('/html/body/div/div[2]/div[2]/div[1]/form/div/div/div/div/div/input[1]'),
	btnGoogleEmailNextSelector: By.xpath('//*[@id="identifierNext"]/div/button'),
	btnGoogleEmailNextHeadlessSelector: By.xpath('/html/body/div/div[2]/div[2]/div[1]/form/div/div/input'),
	txtGooglePasswordSelector: By.xpath('//*[@id="password"]/div[1]/div/div[1]/input'),
	txtGooglePasswordHeadlessSelector: By.xpath('/html/body/div[1]/div[2]/div/div[2]/form/span/div/div[2]/input'),
	btnGooglePasswordNextSelector: By.xpath('//*[@id="passwordNext"]/div/button'),
	btnGooglePasswordNextHeadlessSelector: By.xpath('/html/body/div[1]/div[2]/div/div[2]/form/span/div/input[2]'),
	btnAllowConsentSelector: By.xpath('//button[text()="Allow"]'),
};

const navbar = {
	lblUserNameSelector: By.css('[data-test-id="lblUserName"]'),
	imgNotificationBadgeSelector: By.css('[data-test-id="imgNotificationBadge"]'),
	imgMessageBadgeSelector: By.css('[data-test-id="imgMessageBadge"]'),
	ddUserNavigationDropdownSelector: By.css('[data-test-id="ddUserNavigation"]'),
	optAccountSelector: By.css('[data-test-id="optAccount"]'),
	optToolsSelector: By.css('[data-test-id="optTools"]'),
	optProjectsSelector: By.css('[data-test-id="optProjects"]'),
	optPapersSelector: By.css('[data-test-id="optPapers"]'),
	optCoursesSelector: By.css('[data-test-id="optCourses"]'),
	optDataAccessRequestsSelector: By.css('[data-test-id="optDataAccessRequests"]'),
	optDatasetsSelector: By.css('[data-test-id="optDatasets"]'),
	optCollectionsSelector: By.css('[data-test-id="optCollections"]'),
	optUsersRolesSelector: By.css('[data-test-id="optUsersRoles"]'),
	optLogoutSelector: By.css('[data-test-id="optLogout"]'),
	lnkPublicDashboardSelector: By.css('[data-test-id="lnkPublicDashboard"]'),
	ddUserNavigationSubMenuSelector: By.css('[data-test-id="ddUserNavigationSubMenu"]'),
	ddAddNewEntitySelector: By.css('[data-test-id="addNewEntityDropdown"]'),
	optAddCollectionSelector: By.css('[data-test-id="addNewCollection"]'),
	optAddCourseSelector: By.css('[data-test-id="addNewCourse"]'),
	optAddPaperSelector: By.css('[data-test-id="addNewPaper"]'),
	optAddProjectSelector: By.css('[data-test-id="addNewProject"]'),
	optAddToolSelector: By.css('[data-test-id="addNewTool"]'),
};

const dashboard = {
	lblDashboardDatasetCountSelector: By.css('[data-test-id="dashboard-dataset-count"]'),
	lblDashboardDatasetMetadataPercentSelector: By.css('[data-test-id="dashboard-dataset-metadata-percent"]'),
	lblDashboardUsersMonthlyCountSelector: By.css('[data-test-id="dashboard-users-monthly-count"]'),
	lblDashboardUsersRegisteredPercentSelector: By.css('[data-test-id="dashboard-users-registered-percent"]'),
	lblDashboardSearchesResultsPercentSelector: By.css('[data-test-id="dashboard-searches-results-percent"]'),
	lblDashboardDataAccessRequestsCountSelector: By.css('[data-test-id="dashboard-data-access-requests-count"]'),
	lblDashboardGatewayUptimePercentSelector: By.css('[data-test-id="dashboard-gateway-uptime-percent"]'),
	lblDashboardMetricsLastUpdatedSelector: By.css('[data-test-id="dashboard-metrics-last-updated"]'),
	lstDashboardPopularDatasetsSelector: By.xpath('//div[@data-test-id="topDatasets-dataset"]'),
	lblDashboardPopularDatasetsDatasetNameSelector: By.xpath('.//a[@data-test-id="topDatasets-dataset-name"]'),
	lblDashboardPopularDatasetsDatasetCustodianSelector: By.xpath('.//span[@data-test-id="topDatasets-dataset-publisher"]'),
	lblDashboardPopularDatasetsDatasetCountSelector: By.xpath('.//span[@data-test-id="topDatasets-dataset-request-count"]'),
	lstDashboardPopularSearchesSelector: By.xpath('//div[@data-test-id="topSearches-search"]'),
	lblDashboardPopularSearchTermSelector: By.xpath('.//a[@data-test-id="topSearches-search-term"]'),
	lblDashboardPopularSearchCountSelector: By.xpath('.//span[@data-test-id="topSearches-search-count"]'),
	lblDashboardPopularSearchResultsSelector: By.xpath('.//span[@data-test-id="topSearches-search-results"]'),
	lstDashboardUnmetDemandSearchesSelector: By.xpath('//div[@data-test-id="unmetDemand-search"]'),
	lblDashboardUnmetDemandSearchTermSelector: By.xpath('.//a[@data-test-id="unmetDemand-search-term"]'),
	lblDashboardUnmetDemandSearchCountSelector: By.xpath('.//span[@data-test-id="unmetDemand-search-count"]'),
	lblDashboardUnmetDemandSearchResultsSelector: By.xpath('.//span[@data-test-id="unmetDemand-search-results"]'),
	btnDashboardUnmetDemandDatasetsTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[1]'),
	btnDashboardUnmetDemandToolsTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[2]'),
	btnDashboardUnmetDemandProjectsTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[3]'),
	//btnDashboardUnmetDemandCoursesTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[4]'),
	btnDashboardUnmetDemandPapersTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[5]'),
	btnDashboardUnmetDemandPeopleTabSelector: By.xpath('//*[@data-test-id="unmet-tabs"]/a[6]'),
};

const userAccount = {
	txtFirstNameSelector: By.css('[data-test-id="user-account-first-name"]'),
	valFirstNameSelector: By.css('[data-test-id="user-account-first-name-validation"]'),
	txtLastNameSelector: By.css('[data-test-id="user-account-last-name"]'),
	valLastNameSelector: By.css('[data-test-id="user-account-last-name-validation"]'),
	txtEmailAddressSelector: By.css('[data-test-id="user-account-email-address"]'),
	valEmailAddressSelector: By.css('[data-test-id="user-account-email-address-validation"]'),
	acOrganisationSelector: By.xpath('//div[@data-test-id="user-account-organisation"]/div/div/input[1]'),
	acDomainSelector: By.css('[data-test-id="user-account-domain"]'),
	txtBioSelector: By.css('[data-test-id="user-account-bio"]'),
	txtLinkSelector: By.css('[data-test-id="user-account-link"]'),
	txtOrcidSelector: By.css('[data-test-id="user-account-orcid"]'),
	chkTermsConditionsSelector: By.css('[data-test-id="user-account-terms-conditions"]'),
	chkOptFeedbackSelector: By.css('[data-test-id="user-account-feedback"]'),
	chkOptNewsSelector: By.css('[data-test-id="user-account-news"]'),
	btnSaveUserAccountSelector: By.css('[data-test-id="user-account-save-changes"]'),
	banDevAndImprovementSelector: By.css('[data-test-id="dev-and-improvement"]'),
};

const toolsDashboard = {
	btnAddNewToolSelector: By.css('[data-test-id="add-tool-btn"]'),
};

const addToolPage = {
	txtLinkSelector: By.name('link'),
	txtNameSelector: By.name('name'),
	acTypeSelector: By.xpath('//div[@data-test-id="txtType"]/div/div/input[1]'),
	txtDescriptionSelector: By.xpath('//div[@data-test-id="description"]/textarea'),
	txtAuthorsSelector: By.css('[data-test-id="authors"]'),
	acImplementationSelector: By.xpath('//div[@data-test-id="programmingLanguage.0.programmingLanguage"]/div/div/input[1]'),
	txtVersionSelector: By.name('programmingLanguage.0.version'),
	acUploadersSelector: By.xpath('//div[@data-test-id="uploaders"]/div/div/div/div/input[1]'),
	btnPublishSelector: By.css('[data-test-id="add-tool-publish"]'),
};

const toolPage = {
	banToolAddedSelector: By.css('[data-test-id="tool-added-banner"]'),
	banToolPendingReviewSelector: By.css('[data-test-id="tool-pending-banner"]'),
	lblNameSelector: By.css('[data-test-id="tool-name"]'),
	lblURLSelector: By.css('[data-test-id="tool-page-url"]'),
	lblTypeSelector: By.css('[data-test-id="tool-type"]'),
	lblDescriptionSelector: By.css('[data-test-id="tool-description"]'),
	lblAuthorsSelector: By.css('[data-test-id="tool-authors"]'),
	lblImplementationSelector: By.xpath('//div[@data-test-id="tool-implementation"]/span'),
	lblVersionSelector: By.xpath('//div[@data-test-id="tool-implementation"]/span[2]'),
	lblUploaders1Selector: By.css('[data-test-id="uploader-5348553056662175"]'),
	lblUploaders2Selector: By.css('[data-test-id="uploader-18195711321348120"]'),
};

const papersDashboard = {
	btnAddNewPaperSelector: By.css('[data-test-id="add-paper-btn"]'),
};

const addPaperPage = {
	txtTitleSelector: By.name('name'),
	txtLink1Selector: By.name('document_links.doi.0'),
	txtLink2Selector: By.name('document_links.doi.1'),
	txtLink3Selector: By.name('document_links.doi.2'),
	btnAddLink1Selector: By.css('[data-test-id="add-link-0"]'),
	btnAddLink2Selector: By.css('[data-test-id="add-link-1"]'),
	txtAuthorsSelector: By.css('[data-test-id="authors"]'),
	txtJournalSelector: By.name('journal'),
	txtYearSelector: By.name('journalYear'),
	txtAbstractSelector: By.css('[data-test-id="abstract"]'),
	txtResultsSelector: By.name('resultsInsights'),
	acKeywordsSelector: By.xpath('//div[@data-test-id="keywords"]/div/div/div/div/input[1]'),
	acDomainSelector: By.xpath('//div[@data-test-id="domain"]/div/div/div/div/input[1]'),
	acUploadersSelector: By.xpath('//div[@data-test-id="uploaders"]/div/div/div/div/input[1]'),
	btnPublishSelector: By.css('[data-test-id="add-paper-publish"]'),
};

const editPaperPage = {
	acKeyword1Selector: By.xpath('//div[@data-test-id="keywords"]/div/div/div/div[1]'),
	acDomain1Selector: By.xpath('//div[@data-test-id="domain"]/div/div/div/div[1]'),
	acUploader1Selector: By.xpath('//div[@data-test-id="uploaders"]/div/div/div/div[1]'),
	acUploader2Selector: By.xpath('//div[@data-test-id="uploaders"]/div/div/div/div[2]'),
};

const paperPage = {
	banPaperPendingReviewSelector: By.css('[data-test-id="paper-pending-banner"]'),
	lblNameSelector: By.css('[data-test-id="paper-name"]'),
	lblLink1Selector: By.css('[data-test-id="document-links-doi-0"]'),
	lblLink2Selector: By.css('[data-test-id="document-links-html-0"]'),
	lblLink3Selector: By.css('[data-test-id="document-links-html-1"]'),
	lblAuthorsSelector: By.css('[data-test-id="paper-authors"]'),
	lblJournalSelector: By.css('[data-test-id="paper-journal"]'),
	lblYearSelector: By.css('[data-test-id="paper-year"]'),
	lblAbstractSelector: By.xpath('//span[@data-test-id="paper-abstract"]/p'),
	lblResultsSelector: By.xpath('//span[@data-test-id="paper-results"]/p'),
	lblKeywords1Selector: By.css('[data-test-id="keywords-0"]'),
	lblDomain1Selector: By.css('[data-test-id="domain-0"]'),
	lblUploaders1Selector: By.css('[data-test-id="uploader-5348553056662175"]'),
	lblUploaders2Selector: By.css('[data-test-id="uploader-18195711321348120"]'),
};

const addProjectPage = {
	txtLinkSelector: By.css('[data-test-id="link"]'),
	txtTitleSelector: By.name('name'),
	acTypeSelector: By.xpath('//div[@data-test-id="type"]/div/div/input[1]'),
	txtDescriptionSelector: By.css('[data-test-id="description"]'),
	txtResultsSelector: By.name('resultsInsights'),
	txtLeadResearcherSelector: By.css('[data-test-id="leadResearcher"]'),
	txtAuthorsSelector: By.css('[data-test-id="authors"]'),
	acKeywordsSelector: By.xpath('//div[@data-test-id="keywords"]/div/div/div/div/input[1]'),
	acDomainSelector: By.xpath('//div[@data-test-id="domain"]/div/div/div/div/input[1]'),
	acUploadersSelector: By.xpath('//div[@data-test-id="uploaders"]/div/div/div/div/input[1]'),
	btnPublishSelector: By.css('[data-test-id="add-project-publish"]'),
};

const projectPage = {
	banProjectPendingReviewSelector: By.css('[data-test-id="project-pending-banner"]'),
	lblLinkSelector: By.css('[data-test-id="link"]'),
	lblNameSelector: By.css('[data-test-id="project-name"]'),
	lblTypeSelector: By.css('[data-test-id="project-type"]'),
	lblDescriptionSelector: By.css('[data-test-id="project-description"]'),
	lblResultsSelector: By.css('[data-test-id="project-results"]'),
	lblLeadResearcherSelector: By.css('[data-test-id="project-leadResearcher"]'),
	lblAuthorsSelector: By.css('[data-test-id="project-authors"]'),
	lblKeywords1Selector: By.css('[data-test-id="keywords-0"]'),
	lblDomain1Selector: By.css('[data-test-id="domain-0"]'),
	lblUploaders1Selector: By.css('[data-test-id="uploader-5348553056662175"]'),
	lblUploaders2Selector: By.css('[data-test-id="uploader-18195711321348120"]'),
};

const collectionsDashboard = {
	btnAddNewCollectionSelector: By.css('[data-test-id="add-collection-btn"]'),
};

const addCollectionPage = {
	txtNameSelector: By.css('[data-test-id="collection-name'),
	txtDescriptionSelector: By.css('[data-test-id="collection-description'),
	acKeywordsSelector: By.xpath('//div[@data-test-id="keywords"]/div/div/div/div/input[1]'),
	btnPublishSelector: By.css('[data-test-id="add-collection-publish"]'),
	btnAddRelatedResourceSelector: By.css('[data-test-id="add-resource"]'),
};

const collectionPage = {
	banCollectionAddedSelector: By.css('[data-test-id="collection-added-banner"]'),
	lblNameSelector: By.css('[data-test-id="collectionName"]'),
	lblDescriptionSelector: By.css('[data-test-id="collection-description"]'),
	lblKeywordsSelector: By.css('[data-testid="collectionKeyword0"]'),
	lblRelatedPaperSelector: By.css('[data-test-id="related-paper-object"]'),
};

const accountNavigationMenu = {
	btnDatasetsSelector: By.css('[data-test-id="datasets"]'),
	btnAdvancedSearchSelector: By.css('[data-test-id="advanced-search"]'),
};

const advancedSearchPage = {
	btnAccessAdvancedSearchSelector: By.css('[data-test-id="access-advanced-search"]'),
	btnRequestAccessSelector: By.css('[data-test-id="request-access"]'),
	btnAgreeToTermsSelector: By.css('[data-test-id="agree-to-terms"]'),
	valAgreeTermsSelector: By.css('[data-test-id="advanced-search-terms-conditions-validation"]'),
};

const relatedResourcesModal = {
	txtSearchBarSelector: By.css('[data-test-id="simple-search-bar"]'),
	btnAddResourcesSelector: By.xpath('//*[@id="addResources"]'),
};

const actionBar = {
	btnEditResourceSelector: By.css('[data-test-id="action-bar-edit"]'),
};

const coursesDashboard = {
	btnAddNewCourseSelector: By.css('[data-test-id="add-course-btn"]'),
};

const addCoursePage = {
	txtTitleSelector: By.css('[data-test-id="title"]'),
	txtURLSelector: By.css('[data-test-id="url"]'),
	txtCourseProviderSelector: By.css('[data-test-id="provider"]'),
	txtLocationSelector: By.css('[data-test-id="location"]'),
	txtDescriptionSelector: By.css('[data-test-id="description"]'),
	chkFlexibleDatesSelector: By.css('[data-test-id="flexible-dates"]'),
	ddStudyModeSelector: By.css('[data-test-id="study-mode"]'),
	optFullTimeSelector: By.css('[data-test-id="study-mode-Full-time"]'),
	txtDurationNumberSelector: By.css('[data-test-id="study-duration-number"]'),
	ddDurationMeasureSelector: By.css('[data-test-id="study-duration-measure"]'),
	optDurationYearsSelector: By.css('[data-test-id="duration-measure-Year(s)"]'),
	txtFeeDescriptionSelector: By.css('[data-test-id="fee-description"]'),
	txtFeeAmountSelector: By.css('[data-test-id="fee-amount"]'),
	ddFeePerSelector: By.css('[data-test-id="fee-per"]'),
	optFeePerYearSelector: By.css('[data-test-id="fee-per-Year"]'),
	ddEntryLevelSelector: By.css('[data-test-id="entry-level"]'),
	optEntryLevelBachelorsSelector: By.css('[data-test-id="entry-level-Bachelors"]'),
	txtEntrySubjectSelector: By.css('[data-test-id="entry-subject"]'),
	btnPublishSelector: By.css('[data-test-id="add-course-publish"]'),
};

const coursePage = {
	lblTitleSelector: By.css('[data-test-id="course-title"]'),
	lblURLSelector: By.css('[data-test-id="course-url"]'),
	lblProviderSelector: By.css('[data-test-id="course-provider"]'),
	lblDescriptionSelector: By.css('[data-test-id="course-description"]'),
	lblLocationSelector: By.css('[data-test-id="course-location"]'),
	lblCourseDateSelector: By.css('[data-test-id="course-date"]'),
	lblCourseDurationSelector: By.css('[data-test-id="course-duration"]'),
	lblCourseFeesSelector: By.css('[data-test-id="course-fees"]'),
	lblEntryLevelSelector: By.css('[data-test-id="entry-level"]'),
	lblEntrySubjectSelector: By.css('[data-test-id="entry-subject"]'),
};

const searchBar = {
	txtSearchBarSelector: By.css('[data-test-id="searchbar"]'),
};

const datasetPage = {
	btnRequestAccessSelector: By.css('[data-test-id="dataset-request-access-btn"]'),
};

const datasetModal = {
	btnMakeAnEnquirySelector: By.css('[data-test-id="dar-modal-make-enquiry-btn"]'),
};

const enquiryMessage = {
	txtApplicantNameSelector: By.css('[data-test-id="safepeopleprimaryapplicantfullname"]'),
	txtOrganisationSelector: By.css('[data-test-id="safepeopleprimaryapplicantorganisationname"]'),
	txtEmailSelector: By.css('[data-test-id="safepeopleprimaryapplicantemail"]'),
	txtProjectTitleSelector: By.css('[data-test-id="safeprojectprojectdetailstitle"]'),
	txtProjectAimSelector: By.css('[data-test-id="safeprojectprojectdetailsaimsobjectivesrationale"]'),
	txtFundingSelector: By.css('[data-test-id="funding"]'),
	txtResearchBenefitsSelector: By.css('[data-test-id="safeprojectprojectdetailspublicbenefitimpact"]'),
	chkLinkedDatasetsYesSelector: By.css('[data-test-id="linked-datasets-yes"]'),
	txtLinkedDatasetsSelector: By.css('[data-test-id="safedataotherdatasetslinkadditionaldatasetslinkagedetails"]'),
	chkDatasetPartsNoSelector: By.css('input[data-test-id="dataset-parts-no"]'),
	btnSendMessageSelector: By.css('[data-test-id="send-first-message-btn"]'),
};

const messageItem = {
	txtFirstMessageItemSelector: By.css('[data-test-id="message-item"]'),
};

module.exports = {
	common: common,
	login: login,
	navbar: navbar,
	dashboard: dashboard,
	userAccount: userAccount,
	toolsDashboard: toolsDashboard,
	addToolPage: addToolPage,
	toolPage: toolPage,
	papersDashboard: papersDashboard,
	addPaperPage: addPaperPage,
	editPaperPage: editPaperPage,
	paperPage: paperPage,
	addProjectPage: addProjectPage,
	projectPage: projectPage,
	collectionsDashboard: collectionsDashboard,
	addCollectionPage: addCollectionPage,
	collectionPage: collectionPage,
	accountNavigationMenu: accountNavigationMenu,
	advancedSearchPage: advancedSearchPage,
	relatedResourcesModal: relatedResourcesModal,
	actionBar: actionBar,
	coursesDashboard: coursesDashboard,
	addCoursePage: addCoursePage,
	coursePage: coursePage,
	searchBar: searchBar,
	datasetPage: datasetPage,
	datasetModal: datasetModal,
	enquiryMessage: enquiryMessage,
	messageItem: messageItem,
};
