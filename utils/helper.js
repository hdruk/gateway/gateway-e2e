const urlExist = require('url-exist');

const isLinkAlive = async link => {
    if(link.includes('localhost')){
        return true;
    }
	return await urlExist(link);
};

module.exports = {
	isLinkAlive: isLinkAlive,
};
