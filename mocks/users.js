const users = {
	ADMIN: {
		firstName: 'Test',
		lastName: 'Account',
		role: 'Admin',
		emailAddress: 'hdrtestautomation2@gmail.com',
	},
	USER: {
		firstName: 'Test',
		lastName: 'Account',
		role: 'User',
		emailAddress: 'hdrtestautomation1@gmail.com',
	},
	OA_USER: {
		firstName: 'Test',
		lastName: 'Account',
		role: 'Admin',
		emailAddress: 'hdrtestautomationoa@gmail.com',
	},
};

module.exports = users;
