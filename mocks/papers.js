const papers = {
	PAPER_PAGE: {
		title: 'Test paper created from E2E repo ' + new Date().toGMTString(),
		link1: 'https://doi.org/10.1111/bjd.18046',
		link2: 'https://testlink.com/',
		link3: 'https://europepmc.org/articles/PMC6972719?pdf=render',
		authors: 'Dr. J Smith, Prof B Small',
		journal: 'Test journal name',
		year: '2021',
		abstract: 'Example text for paper abstract',
		results: 'Example text for paper results/insights',
		keywords: 'Genetic Testing',
		domain: 'Musculoskeletal',
		uploader1: 'Test Account',
		uploader2: 'Richard Hobbs'
	},
};

module.exports = papers;
