const courses = {
	COURSE_PAGE: {
		title: 'Test course created from E2E repo ' + new Date().toGMTString(),
		url: 'http://hdruk.co.uk',
		courseProvider: 'Test provider',
		location: 'Test location',
		description: 'Test description',
		flexibleDates: true,
		date: 'Flexible',
		studyMode: 'Full-time',
		durationNumber: '3',
		durationMeasure: 'Year(s)',
		feeDescription: 'Cost of course',
		feeAmount: '1000',
		feePer: 'Year',
		feePerLabel: 'year',
		entryLevel: 'Bachelors',
		entrySubject: 'Computing Science',
	},
};

module.exports = courses;
