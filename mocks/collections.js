const papers = {
	COLLECTION_PAGE: {
		name: 'Test collection created from E2E repo ' + new Date().toGMTString(),
		description: 'Example text for collection abstract',
		keywords: 'covid',
	},
};

module.exports = papers;
