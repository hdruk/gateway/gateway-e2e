const projects = {
	PROJECT_PAGE: {
		link: 'https://testlink.org/',
		title: 'Test project created from E2E repo ' + new Date().toGMTString(),
		type: 'API',
		description: 'Example text for project abstract',
		results: 'Example text for project results/insights',
		leadResearcher: 'Dr. J Danaher',
		authors: 'Dr. J Smith, Prof B Small',
		keywords: 'Genetic Testing',
		domain: 'Musculoskeletal',
		uploader1: 'Test Account',
		uploader2: 'Richard Hobbs',
	},
};

module.exports = projects;
