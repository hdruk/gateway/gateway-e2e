const tools = {
	TOOL_PAGE: {
		link: 'https://testlink.com/',
		name: 'Test tool created from E2E repo',
		type: 'API',
		description: 'A tool for testing purposes',
		authors: 'Dr. J Smith, Prof B Small',
		implementation: 'Java',
		version: '9',
		uploader1: 'Test Account',
		uploader2: 'Richard Hobbs'
	},
};

module.exports = tools;
