const firstMessage = {
	FIRST_MESSAGE: {
		applicantName: 'Pascal Pfiffner',
		organisation: 'PA Consulting',
		email: 'pascal.pfiffner@paconsulting.com',
		projectTitle: 'Test project title',
		projectAim: 'Test project aim which briefly explains the purpose of my research and why I require this dataset',
		funding: 'Test information information on whether my project is funded',
		researchBenefits: 'Test evidence of how my research will benefit the health and social care system',
		linkedDatasets: 'Test linked dataset 1, test linked dataset 2',
	},

	MESSAGE_ITEM: {
		message:
			'Name: Pascal Pfiffner\n Organisation: PA Consulting\n Email: pascal.pfiffner@paconsulting.com\n Project title: Test project title\n Research aim or question: Test project aim which briefly explains the purpose of my research and why I require this dataset\n Datasets of interest: HDR UK Papers & Preprints\n Are there other datasets you would like to link with the ones listed above?: Yes\n Name or description of the linked datasets: Test linked dataset 1, test linked dataset 2\n Do you know which parts of the dataset you are interested in?: No\n Funding: Test information information on whether my project is funded\n Potential research benefits: Test evidence of how my research will benefit the health and social care system',
	},
};

module.exports = firstMessage;
